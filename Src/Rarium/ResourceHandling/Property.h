#ifndef _RPROPERTY_

#define _RPROPERTY_

#include <Rarium/Core.h>
#include <string>
#include <vector>

namespace Rarium
{
	class Property
	{
	public:
		/**
		*	Build a new property from a set of values
		*	@param name Name of the property
		*	@param values Set of values of the property
		*/
		Property(std::string name = "", std::vector<std::string> values = std::vector<std::string>())
		{
			this->name = name;
			this->values = values;
		}

		/**
		*	Build a new property from a single value
		*	@param name Name of that property
		*	@param value Value of that property
		*/
		Property(std::string name, std::string value)
		{
			this->name = name;
			this->values.push_back(value);
		}

		/**
		*	Add a value to this property
		*	@param value Value to add
		*/
		inline void AddValue(std::string value) 
		{
			this->values.push_back(value);
		}

		/**
		*	Add a set of values to that properties
		*	@param values Set of values to add
		*/
		inline void AddValues(std::vector<std::string> values)
		{
			for(std::string &value : values)
				this->values.push_back(value);
		}

		/**
		*	Get the value of that property in case it is a single value property
		*	@return The value of that property
		*/
		inline std::string GetValue() 
		{
			return values.front();
		}

		/**
		*	Get the set of values of that property
		*	@return The set of values of that property
		*/
		inline std::vector<std::string> GetValues() 
		{
			return values;
		}

		/**
		*	Get the name of this property
		*	@return Name of this property
		*/
		inline std::string GetName() { return name; }

		/**
		*	To know if two properties are equal, it only compare the property name
		*/
		inline bool operator==(Property other) { return name == other.name; }

	protected:
		std::string name;///< Name of the property
		std::vector<std::string> values;///< All the values of the property
	};
}

#endif