#include <Rarium/ResourceHandling/Resource.h>

namespace Rarium
{
	Resource::Resource(std::string name)
	{
		this->name = name;
		this->inMemory = false;
	}

	Resource::~Resource() 
	{ 
		if(inMemory) 
			Release();
	}

	Property Resource::GetProperty(std::string name)
	{
		return properties[name];
	}

	void Resource::AddProperty(Property prop)
	{
		properties.insert(std::make_pair(prop.GetName(),prop));
	}
}