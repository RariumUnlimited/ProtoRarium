#include <Rarium/ResourceHandling/Catalog.h>
#include <Rarium/Engine/Engine.h>
#include <sstream>

namespace Rarium
{
	Catalog::Catalog()
	{

	}

	void Catalog::Purge()
	{
		//Delete every resources we have loaded so far
		for(std::map<std::string,Resource*>::iterator it = resources.begin(); it != resources.end(); it++)
		{
			delete it->second;
		}

		resources.clear();
		Log("Catalog: Purge completed");
	}

	void Catalog::SetResourcePropertiesAndLoad(Resource* resource)
	{
		//Open the descriptor file
		File file = Engine::ReadFile(resource->GetResourceDirectory()+resource->Name()+".rds");
		
		if(file.Size == 0)
			throw Exception("Unable to open resource descriptor file : "+resource->GetResourceDirectory()+resource->Name()+".rds");
		std::istringstream stream(std::string(file.Data,file.Size));
		std::string currentName = "";
		std::vector<std::string> currentValues;

		while(!stream.eof())
		{
			std::string prop;
			std::getline(stream,prop);

			if(prop == "" || ( prop.length() >= 2 && prop[0] == '/' && prop[1] == '/'))
				continue;

			while(prop.back() == '\r' || prop.back() == '\n')
			{
				prop.pop_back();
			}

			//If @ then it's a new property else it is a value of a multi value property
			if(prop.front() == '@')
			{
				//We had a property waiting
				if(currentName != "")
				{
					resource->AddProperty(Property(currentName,currentValues));
					currentValues.clear();
					currentName = "";
				}
				
				prop.erase(0,1);

				//If line end with = it is a multi value property, else it is a single value property
				if(prop.back() == '=')
				{
					prop.pop_back();
					currentName = prop;
				}
				else
				{
					std::vector<std::string> propSplit = Split(prop,'=');
					std::string key = propSplit.front();
					std::string value = propSplit.back();
					resource->AddProperty(Property(key,value));
				}
			}
			else
				currentValues.push_back(prop);
		}

		//We had a property waiting
		if(currentName != "")
		{
			resource->AddProperty(Property(currentName,currentValues));
		}

		delete file.Data;

		//We are checking that all required properties are available
		for(std::set<std::string>::iterator it = resource->GetRequiredProperties().begin(); it != resource->GetRequiredProperties().end(); it++)
		{
			if(resource->GetProperty((*it)).GetName() == "")
			{
				throw Exception("Required property ("+(*it)+") not found in "+resource->Name());
			}
		}

		resource->Load();
	}

	Catalog Catalog::instance;
}