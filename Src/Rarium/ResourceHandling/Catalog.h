#ifndef _RCATALOG_

#define _RCATALOG_

#include <Rarium/Core.h>
#include <Rarium/ResourceHandling/Resource.h>
#include <Rarium/Rendering/IRenderer.h>
#include <map>
#include <stack>
#include <mutex>

namespace Rarium
{
	/**
	*	Resource catalog that must be use to handle resource
	*/
	class Catalog
	{
	public:
		/**
		*	Load the properties of a resource from its descriptor file, and load it in cpu
		*	@param resource Resource that we want to load properties
		*/
		void SetResourcePropertiesAndLoad(Resource* resource);
		/**
		*	Release all the resource of the catalog and unregistered them
		*/
		void Purge();
		/**
		*	Get the instance of our catalog
		*	@return The only instance of the catalog class
		*/
		static inline Catalog& Get() { return instance; }

		/**
		*	Register a new resource to be add to the loading list or return the existing one
		*	@param Name of the resource to register
		*	@return The newly created resource (but not yet loaded) or the already existing resource.
		*/
		template<typename C>
		C* RegisterResource(std::string name)
		{
			//Query our map to see if our resource exist
			std::map<std::string,Resource*>::iterator it = resources.find(name);
			C* temp = nullptr;
			
			//if it exist we return it else we create a new one
			if(it != resources.end())
				temp = (C*)it->second;
			else
			{
				resources[name] = temp = new C(name);
				SetResourcePropertiesAndLoad(temp);
			}
			return temp;
		}

	protected:
		std::map<std::string,Resource*> resources;///< All resources registered by the user
		
	private:
		/**
		*	Build a new catalog object
		*/
		Catalog();
		static Catalog instance;///< The only instance of our catalog
	};
}

#endif