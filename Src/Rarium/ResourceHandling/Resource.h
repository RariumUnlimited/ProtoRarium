#ifndef _RRESOURCE_

#define _RRESOURCE_

#include <GL/glew.h>
#include <Rarium/Core.h>
#include <string>
#include <map>
#include <set>
#include <Rarium/ResourceHandling/Property.h>

namespace Rarium
{
	/**
	*	Resource used by the engine (could be 3D model, texture, ...)
	*	Each resource have at least one file : a Rarium Descriptor file (.rds) that contains properties of the resource.
	*	The rds file is constructed this way : One line = one propertiy. The first part of the line is the property name, The second part is the property value.
	*	The name and value a separeted by the folowing character sequence : "="
	*/
	class Resource
	{
	public:
		/**
		*	Build a new resource object
		*	@param name Name of the resource
		*/
		Resource(std::string name);
		/**
		*	Destructor
		*/
		virtual ~Resource();
		/**
		*	Get the value of a property
		*	@param name Name of the property we want
		*	@return The property that has this name
		*/
		Property GetProperty(std::string name);
		/**
		*	Add a property to this resource
		*	@param prop A new property
		*/
		void AddProperty(Property prop);
		/**
		*	Get the set of required properties for this resource
		*	@return Required properties for this resource
		*/
		inline std::set<std::string>& GetRequiredProperties() { return requiredProperties; }
		/**
		*	Get the directory in where our resource is
		*	@return Directory where our resource is
		*/
		virtual std::string GetResourceDirectory() = 0;
		/**
		*	Function to implement by child classes to load the resource in CPU Memory.
		*/
		virtual void Load() = 0;
		/**
		*	Function to implement by child classe to unload the resource from CPU Memory.
		*/
		virtual void Release() {}
		/**
		*	Get the name of the resource
		*	@return The name of the resource
		*/
		inline std::string Name() { return name; }
		/**
		*	To know if the resource is in the cpu memory
		*	@return True if the resource is in the cpu memory
		*/
		inline bool IsInMemory() { return inMemory; }

	protected:
		std::map<std::string, Property> properties;///< Properties of the resource (read from the descriptor file). [Name,Value].
		std::set<std::string> requiredProperties;///< Properties that must be present in the properties map for the resource to work. Must be filled by child class.
		std::string name;///< Name of that resource
		bool inMemory;///< If the resource has been loaded on cpu
	};
}

#endif