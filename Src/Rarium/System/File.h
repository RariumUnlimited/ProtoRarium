#ifndef _RFILE_

#define _RFILE_

/**
*	Reprensent a file read from the asset dir
*	/!\ It is not the engine duty to delete the data buffer
*/
struct File
{
	File()
	{
		Data = nullptr;
		Size = 0;
	}

	char* Data;///<Real size of Data should be Size + 1, with Data[Size] = '\0'
	std::size_t Size;///<Size in byte the data with the \0
};

#endif