#include <Rarium/System/Clock.h>

namespace Rarium
{
	Clock::Clock()
	{
		lastCheck = clock();
	}
	
	float Clock::GetTotalTime()
	{
		return ((float)clock())/((float)CLOCKS_PER_SEC);
	}

	float Clock::GetElapsedTime()
	{
		clock_t temp = clock();
		float result = ((float)(temp - lastCheck))/((float)CLOCKS_PER_SEC);
		lastCheck = temp;
		return result;
	}

	void Clock::BeginRecording()
	{
		recordBegin = clock();
	}

	float Clock::EndRecording()
	{
		return ((float)(clock() - recordBegin))/((float)CLOCKS_PER_SEC);
	}

	Clock Clock::instance;
}