#ifndef _RCLOCK_

#define _RCLOCK_

#include <Rarium/Core.h>
#include <ctime>


namespace Rarium
{
	/**
	*	Singleton class that measure time while the program is running
	*/
	class Clock
	{
	public:

		/**
		*	Return for how long the program has been running
		*	@return The total time the program has been running in second
		*/
		float GetTotalTime();
		/**
		*	Get how many seconds passed since the last call of GetElapsedTime
		*	@return The elapsed time in seconds
		*/
		float GetElapsedTime();

		/**
		*	Begin recording time, call EndRecording after to know how many seconds passed between the two call.
		*/
		void BeginRecording();

		/**
		*	Get the recording resut.
		*	@return How many seconds between BeginRecording and this call
		*/
		float EndRecording();

		/**
		*	Get our Clock.
		*	@return Reference to our clock instance
		*/
		static inline Clock& Get() { return instance; }

	protected:

		clock_t lastCheck;///< Time at the last call of GetElapsedTime
		clock_t recordBegin;///< Time at which we called BeginRecording

	private:
		Clock();///< Build a new clock
		static Clock instance;///< Instance of our clock
	};
}

#endif