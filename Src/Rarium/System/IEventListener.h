#ifndef _RIEVENTLISTENER_

#define _RIEVENTLISTENER_

namespace Rarium
{
	/**
	*	Interface to allow class listening Window event
	*/
	class IEventListener
	{
	public:
		/**
		*	Called when the window wants closing
		*/
		virtual void OnWindowClose() = 0;
		/**
		*	Call when the window got resized
		*	@param width New width of the window
		*	@param height New height of the window
		*/
		virtual void OnWindowResize(int width, int height) = 0;
	};
}

#endif