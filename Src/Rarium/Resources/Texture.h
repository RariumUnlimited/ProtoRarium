#ifndef _RTEXTURE_

#define _RTEXTURE_

#include <Rarium/Core.h>
#include <Rarium/ResourceHandling/Resource.h>
#include <gli/gli.hpp>

namespace Rarium
{
	/**
	*	Texture resource. Always 3 components. Must be 24 bits uncompressed TGA file.
	*/
	class Texture : public Resource
	{
	public:
		/**
		*	Build a new texture object
		*	@param name Name of the texture
		*/
		Texture(std::string name);
		/**
		*	Function to implement by child classes to load the resource in CPU Memory.
		*/
		virtual void Load();
		/**
		*	Function to implement by child classe to unload the resource from CPU Memory.
		*/
		virtual void Release();
		/**
		*	Get the directory in where textures are stored
		*	@return Texture directory
		*/
		virtual std::string GetResourceDirectory();
		/**
		*	Get the data of the texture
		*	@return Pixel data for this texture
		*/
		inline gli::texture2D* GetData() { return data; }

	protected:
		gli::texture2D* data;///< Where our texture data is stored
	};
}

#endif