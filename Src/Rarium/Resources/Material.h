#ifndef _RMATERIAL_

#define _RMATERIAL_

#include <Rarium/Core.h>
#include <Rarium/Resources/Texture.h>

namespace Rarium
{
	/**
	*	Material resource (generaly compose of 3 texture : diffuse, specular and gloss).
	*/
	class Material : public Resource
	{
	public:
		/**
		*	Build a new material object
		*	@param name Name of the texture
		*/
		Material(std::string name);

		/**
		*	Get the diffuse texture
		*	@return The diffuse texture
		*/
		inline Texture* GetDiffuse() { return diffuse; }
		/**
		*	Get the specular texture
		*	@return The specular texture if available, nullptr otherwise
		*/
		inline Texture* GetSpecular() { return specular; }
		/**
		*	Get the gloss texture
		*	@return The gloss texture if available, nullptr otherwise
		*/
		inline Texture* GetGloss() { return gloss; }

		/**
		*	Function to implement by child classes to load the resource in CPU Memory.
		*/
		virtual void Load();
		/**
		*	Function to implement by child classe to unload the resource from CPU Memory.
		*/
		virtual void Release();
		
		/**
		*	Get the directory in where materials are stored
		*	@return Material directory
		*/
		virtual std::string GetResourceDirectory();

	protected:
		Texture* diffuse;///< Diffuse map of the material
		Texture* specular;///< Specular map of the material
		Texture* gloss;///< Gloss map of the material
	};
}

#endif