#include <Rarium/Resources/Actor.h>
#include <Rarium/ResourceHandling/Catalog.h>

namespace Rarium
{
	Actor::Actor(std::string name) : Resource(name)
	{
		mesh = nullptr;
		material = nullptr;
		requiredProperties.insert("Material");
		requiredProperties.insert("Mesh");
	}

	void Actor::Load()
	{
		//Register all the texture needed to this material
		material = Catalog::Get().RegisterResource<Material>(properties["Material"].GetValue());
		mesh = Catalog::Get().RegisterResource<Mesh>(properties["Mesh"].GetValue());

		inMemory = true;
	}

	void Actor::Release()
	{
		if(inMemory)
		{
			material->Release();
			mesh->Release();
			inMemory = false;
		}
	}


	std::string Actor::GetResourceDirectory()
	{
		return "Actors/";
	}
}