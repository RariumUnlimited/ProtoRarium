#include <Rarium/Resources/Cubemap.h>
#include <Rarium/Engine/Engine.h>

#include <sstream>

namespace Rarium
{
	Cubemap::Cubemap(std::string name) : Resource(name)
	{
		data = nullptr;
	}

	void Cubemap::Load()
	{
		if(!inMemory)
		{
			File file = Engine::ReadFile(GetResourceDirectory()+name+".dds");

			std::string fileContent(file.Data,file.Size);
			std::istringstream filestream(fileContent);

			gli::storage storage = gli::load_dds(&filestream);

			data = new gli::textureCube(storage);

			delete file.Data;

			AddProperty(Property("Width", ToString(data->dimensions().x)));
			AddProperty(Property("Height", ToString(data->dimensions().y)));

			inMemory = true;
		}
	}


	void Cubemap::Release()
	{
		if(inMemory)
		{
			delete data;
			inMemory = false;
		}
	}


	std::string Cubemap::GetResourceDirectory()
	{
		return "Materials/";
	}

}