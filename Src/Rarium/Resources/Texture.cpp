#include <Rarium/Resources/Texture.h>
#include <Rarium/Engine/Engine.h>

#include <sstream>

namespace Rarium
{
	Texture::Texture(std::string name) : Resource(name)
	{
		data = nullptr;
	}

	void Texture::Load()
	{
		if(!inMemory)
		{
			File file = Engine::ReadFile(GetResourceDirectory()+name+".dds");

			std::string fileContent(file.Data,file.Size);
			std::istringstream filestream(fileContent);

			gli::storage storage = gli::load_dds(&filestream);
			
			data = new gli::texture2D(storage);

			delete file.Data;

			AddProperty(Property("Width", ToString(data->dimensions().x)));
			AddProperty(Property("Height", ToString(data->dimensions().y)));

			inMemory = true;
		}
	}


	void Texture::Release()
	{
		if(inMemory)
		{
			delete data;
			inMemory = false;
		}
	}


	std::string Texture::GetResourceDirectory()
	{
		return "Materials/";
	}

}