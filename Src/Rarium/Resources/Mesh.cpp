#include <Rarium/Resources/Mesh.h>
#include <Rarium/Engine/Engine.h>
#include <sstream>

namespace Rarium
{
	Mesh::Mesh(std::string name) : Resource(name)
	{
		requiredProperties.insert("VertexCount");
		requiredProperties.insert("IndexCount");
		vertices = nullptr;
		indexes = nullptr;
	}

	void Mesh::Load()
	{
		std::istringstream(GetProperty("IndexCount").GetValue()) >> indexCount;
		std::istringstream(GetProperty("VertexCount").GetValue()) >> vertexCount;

		File fileIndex = Engine::ReadFile(GetResourceDirectory()+name+".rin");
		File fileVertex = Engine::ReadFile(GetResourceDirectory()+name+".rve");

		if(fileIndex.Size > 0)
		{
			if(fileVertex.Size > 0)
			{
				indexes = (unsigned int*)fileIndex.Data;
				vertices = (VertexData*)fileVertex.Data;

			}
			else
			{
				delete[] fileIndex.Data;
				throw Exception("Mesh loading failed, bad vertex file : "+name);
			}
		}
		else
		{
			throw Exception("Mesh loading failed, bad index file : "+name);
		}

		inMemory = true;
	}

	void Mesh::Release()
	{
		if(inMemory)
		{
			if(vertices != nullptr)
			{
				delete[] vertices;
				vertices = nullptr;
			}
			if(indexes != nullptr)
			{
				delete[] indexes;
				indexes = nullptr;
			}
			inMemory = false;
		}
	}

	std::string Mesh::GetResourceDirectory()
	{
		return "Meshs/";
	}
}