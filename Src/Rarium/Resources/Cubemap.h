#ifndef _RCUBEMAP_

#define _RCUBEMAP_

#include <Rarium/Core.h>
#include <Rarium/ResourceHandling/Resource.h>
#include <gli/gli.hpp>

namespace Rarium
{
	/**
	*	Cubemap resource.
	*/
	class Cubemap : public Resource
	{
	public:
		/**
		*	Build a new texture object
		*	@param name Name of the texture
		*/
		Cubemap(std::string name);
		/**
		*	Function to implement by child classes to load the resource in CPU Memory.
		*/
		virtual void Load();
		/**
		*	Function to implement by child classe to unload the resource from CPU Memory.
		*/
		virtual void Release();
		/**
		*	Get the directory in where textures are stored
		*	@return Texture directory
		*/
		virtual std::string GetResourceDirectory();
		/**
		*	Get the data of the texture
		*	@return Pixel data for this texture
		*/
		inline gli::textureCube* GetData() { return data; }

	protected:
		gli::textureCube* data;///< Where our texture data is stored
	};
}

#endif