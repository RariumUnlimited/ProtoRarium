#include <Rarium/Resources/Technique.h>
#include <Rarium/Engine/Engine.h>
#include <Rarium/Rendering/RES.h>

namespace Rarium
{
	Technique::Technique(std::string name) : Resource(name)
	{
	}

	void Technique::Load()
	{
		vertexShaderFile = Engine::ReadFile(GetResourceDirectory()+name+".vs.glsl");
		fragmentShaderFile = Engine::ReadFile(GetResourceDirectory()+name+".fs.glsl");

		inMemory = true;
	}

	void Technique::Release()
	{
		if(inMemory)
		{
			if(vertexShaderFile.Data != nullptr)
			{
				delete[] vertexShaderFile.Data;
				vertexShaderFile.Data = nullptr;
			}
			if(fragmentShaderFile.Data != nullptr)
			{
				delete[] fragmentShaderFile.Data;
				fragmentShaderFile.Data = nullptr;
			}
			inMemory = false;
		}
	}

	std::string Technique::GetResourceDirectory()
	{
		return "Techniques/";
	}
}