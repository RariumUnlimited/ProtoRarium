#include <Rarium/Resources/Material.h>
#include <Rarium/ResourceHandling/Catalog.h>

namespace Rarium
{
	Material::Material(std::string name) : Resource(name)
	{
		diffuse = nullptr;
		specular = nullptr;
		gloss = nullptr;
		requiredProperties.insert("Diffuse");
	}

	void Material::Load()
	{
		//Register all the texture needed to this material
		diffuse = Catalog::Get().RegisterResource<Texture>(properties["Diffuse"].GetValue());

		if (properties.find("Specular") != properties.end())
			specular = Catalog::Get().RegisterResource<Texture>(properties["Specular"].GetValue());

		if (properties.find("Gloss") != properties.end())
			gloss = Catalog::Get().RegisterResource<Texture>(properties["Gloss"].GetValue());

		inMemory = true;
	}


	void Material::Release()
	{
		if(inMemory)
		{
			diffuse->Release();
			specular->Release();
			gloss->Release();
			inMemory = false;
		}
	}


	std::string Material::GetResourceDirectory()
	{
		return "Materials/";
	}
}