#ifndef _RMESH_

#define _RMESH_

#include <Rarium/Core.h>
#include <Rarium/ResourceHandling/Resource.h>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <vector>

using namespace glm;

namespace Rarium
{
	/**
	*	Data about a vertex
	*/
	struct VertexData
	{
		vec3 Position;///< Object space position of the vertex
		vec2 TexCoord;///< Texture coordinates for the vertex
		vec3 Normal;///< Normal at this vertex
	};

	/**
	*	3D mesh read from a rve and rin file
	*/
	class Mesh : public Resource
	{
	public:
		/**
		*	Build a new mesh object
		*	@param name Name of the mesh
		*/
		Mesh(std::string name);

		/**
		*	Get how many vertices we have in our mesh
		*	@return The number of vertices in our mesh
		*/
		inline unsigned int GetVertexCount() { return vertexCount; }
		/**
		*	Get how many indexes we have in our mesh
		*	@return The number of indexes in our mesh
		*/
		inline unsigned int GetIndexCount() { return indexCount; }
		/**
		*	Get all the vertices of our mesh
		*	@return Our mesh vertices
		*/
		inline VertexData* GetVertices() { return vertices; }
		/**
		*	Get all the indexes of our mesh
		*	@return Our mesh indexes
		*/
		inline unsigned int* GetIndexes() { return indexes; }
		
		/**
		*	Function to implement by child classes to load the resource in CPU Memory.
		*/
		virtual void Load();
		/**
		*	Function to implement by child classe to unload the resource from CPU Memory.
		*/
		virtual void Release();
		
		/**
		*	Get the directory in where textures are stored
		*	@return Texture directory
		*/
		virtual std::string GetResourceDirectory();
		
	protected:
		VertexData* vertices;///< Vertex array of the mesh
		unsigned int* indexes;///< Indexes array of the mesh
		unsigned int vertexCount;///< How many vertex we have
		unsigned int indexCount;///< How many index we have
	};
}

#endif