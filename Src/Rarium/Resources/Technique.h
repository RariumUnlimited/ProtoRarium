#ifndef _RTECHNIQUE_

#define _RTECHNIQUE_

#include <Rarium/Core.h>
#include <Rarium/ResourceHandling/Resource.h>
#include <Rarium/System/File.h>

namespace Rarium
{
	/**
	*	GLSL technique used for rendering
	*/
	class Technique : public Resource
	{
	public:
		/**
		*	Build a new technique object
		*	@param name Name of the technique
		*/
		Technique(std::string name);
		
		/**
		*	Get the source of the vertex shader for this technique
		*	@return Source read from the shader file
		*/
		inline char* GetVertexShaderSource() { return vertexShaderFile.Data; }
		/**
		*	Get the source of the fragment shader for this technique
		*	@return Source read from the shader file
		*/
		inline char* GetFragmentShaderSource() { return fragmentShaderFile.Data; }

		/**
		*	Function to implement by child classes to load the resource in CPU Memory.
		*/
		virtual void Load();
		/**
		*	Function to implement by child classe to unload the resource from CPU Memory.
		*/
		virtual void Release();

		virtual std::string GetResourceDirectory();


	protected:
		File vertexShaderFile;///< File containing the source code for the vertex shader
		File fragmentShaderFile;///< File containing the source code for the fragment shader
	};
}

#endif