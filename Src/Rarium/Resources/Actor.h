#ifndef _RACTOR_

#define _RACTOR_

#include <Rarium/Core.h>
#include <Rarium/Resources/Material.h>
#include <Rarium/Resources/Mesh.h>
#include <Rarium/Resources/Technique.h>

namespace Rarium
{
	/**
	*	Actor of the 3D world : a mesh, a material
	*/
	class Actor : public Resource
	{
	public:
		/**
		*	Build a new actor object
		*	@param name Name of the actor
		*/
		Actor(std::string name);

		/**
		*	Get the material of this actor
		*	@return Material of this actor
		*/
		inline Material* GetMaterial() { return material; }
		/**
		*	Get the mesh of this actor
		*	@return Mesh of this actor
		*/
		inline Mesh* GetMesh() { return mesh; }
		/**
		*	Set the material of this actor
		*	@param material The new material of this actor
		*/
		inline void SetMaterial(Material* material) { this->material = material; }
		/**
		*	Set the mesh of this actor
		*	@param mesh The new mesh of this actor
		*/
		inline void SetMesh(Mesh* mesh) { this->mesh = mesh; }
		
		/**
		*	Function to implement by child classes to load the resource in CPU Memory.
		*/
		virtual void Load();
		/**
		*	Function to implement by child classe to unload the resource from CPU Memory.
		*/
		virtual void Release();

		/**
		*	Get the directory in where actor are stored
		*	@return Actor directory
		*/
		virtual std::string GetResourceDirectory();

	protected:
		Material* material;///< Material of this actor
		Mesh* mesh;///< Mesh of this actor
	};
}

#endif