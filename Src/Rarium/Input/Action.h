#ifndef _RACTION_

#define _RACTION_

namespace Rarium
{
	/**
	*	Represent action that can be performed by the user
	*/
	enum Action
	{
		Fire = 0,
		Missile,
		ChangeWeapon,
		ChangeMissile,
		LinkGun,
		SpecialAction,
		Menu,
		Map,
		Target,
		Boost,
		Interact,
		Cancel
	};
}

#endif