#ifndef _RINPUTSTATE_

#define _RINPUTSTATE_

#include <Rarium/Input/Action.h>
#include <map>
#include <glm/vec2.hpp>

using namespace glm;

namespace Rarium
{
	class Engine;

	/**
	*	Represent the state of the user input
	*/
	struct InputState
	{
		std::map<Action,bool> Actions;///< Actions and their status
		
		float Yaw;//Yaw ranging from -1 to 1 (Right stick X)
		float Pitch;//Pitch ranging from -1 to 1 (Right stick Y)
		float Roll;//Roll or strafe ranging from -1 to 1 (Left stick X)
		float Throttle;//Throttle ranging from -1 (full reverse) to 1 (full forward) (Left trigger, Left shoulder)

	};
}

#endif