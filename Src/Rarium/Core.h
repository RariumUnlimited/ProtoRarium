#ifndef _RCORE_

#define _RCORE_

#include <SDL/SDL.h>
#include <Rarium/Util/Exception.h>
#include <Rarium/Util/Util.h>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifdef _DEBUG
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif  // _DEBUG

#include <Rarium/Util/Log.h>

#endif