#include <Rarium/Rendering/RES.h>
#include <Rarium/System/Clock.h>
#include <cstdlib>
#include <cmath>
#include <Rarium/ResourceHandling/Catalog.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Rarium/Engine/FixedCamera.h>
#include <Rarium/Util/Log.h>
#include <Rarium/Util/Exception.h>
#include <Rarium/Util/Util.h>


#include <iostream>

namespace Rarium
{
	bool RES::Init(unsigned int width, unsigned int height)
	{
		this->width = width;
		this->height = height;

		glewExperimental = true;
		GLenum err = glewInit();

		if (GLEW_OK != err)
		{
			throw Exception("Unable to init GLEW");
		}
		glGetError();

		//Get context info

		Log("Renderer: GL Info : ");
		Log("Renderer:" + ToString(glGetString(GL_VENDOR)));
		Log("Renderer:" + ToString(glGetString(GL_RENDERER)));
		Log("Renderer:" + ToString(glGetString(GL_VERSION)));
		Log("Renderer:" + ToString(glGetString(GL_SHADING_LANGUAGE_VERSION)));

		InitStrToPfn();

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glClearColor(0, 0, 0, 1);
		glClearDepthf(1.0);

		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

		skybox.Batch = nullptr;
		skybox.Instance = nullptr;

		ambient = vec3(0.0, 0.0, 0.0);

		//Get the list of extensions available
		std::set<std::string> extensions;
		
		int numExt;
		glGetIntegerv(GL_NUM_EXTENSIONS, &numExt);

		for (int i = 0; i < numExt; i++)
		{
			extensions.insert(ToString(glGetStringi(GL_EXTENSIONS,i)));
		}

		//Check that all extensions needed are present
		if (extensions.find("GL_ARB_shader_draw_parameters") == extensions.end())
			throw Exception("GL_ARB_shader_draw_parameters is not available !");

		if (extensions.find("GL_NV_draw_texture") == extensions.end())
			throw Exception("GL_NV_draw_texture is not available !");

		return true;
	}


	void RES::DrawFrame(float delta)
	{
		UpdateModelMatrix();
		ShadowPass();
		ScenePass();
		CheckGLError();
		GLuint tex;
		/*glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D, tex);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, 1024, 1024);
		glBindTexture(GL_TEXTURE_2D_ARRAY, lightBuffer.ShadowMapTexArray);
		glCopyImageSubData(lightBuffer.ShadowMapTexArray, GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, tex, GL_TEXTURE_2D, 0, 0, 0, 0, 1024, 1024, 1);*/
		CheckGLError();
		//glDrawTextureNV(tex, 0, 0, 0, 1024, 1024, 0, 0, 0, 1, 1);
		CheckGLError();
	}

	void RES::ScenePass()
	{
		for (CameraData* camera : cameras)
		{
			RenderData renderData;
			renderData.Camera = camera;

			glBindFramebuffer(GL_FRAMEBUFFER, camera->Fbo);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glViewport(0, 0, camera->BaseCamera->GetViewport().x, camera->BaseCamera->GetViewport().y);

			//Build camera matrix
			projMatrix = glm::perspective(camera->BaseCamera->GetFieldOfView(), float(camera->BaseCamera->GetViewport().x) / float(camera->BaseCamera->GetViewport().y), camera->BaseCamera->GetNearPlaneDistance(), camera->BaseCamera->GetFarPlaneDistance());
			viewMatrix = glm::lookAt(camera->BaseCamera->GetPosition(), camera->BaseCamera->GetLookAt(), camera->BaseCamera->GetUp());


			for (std::map<BatchData*, std::vector<InstanceData*>>::iterator it = batches.begin(); it != batches.end(); it++)
			{
				std::vector<InstanceData*> instances = it->second;
				renderData.Batch = it->first;

				glUseProgram(it->first->Technique->Program);

				glBindVertexArray(renderData.Batch->Vao);

				//Setup uniform
				for (unsigned int j = 0; j < it->first->Technique->CameraUniforms.size(); j++)
					(this->*it->first->Technique->CameraUniforms[j])(renderData);

				for (unsigned int j = 0; j < it->first->Technique->Textures.size(); j++)
					(this->*it->first->Technique->Textures[j])(renderData);

				for (unsigned int j = 0; j < it->first->Technique->GlobalUniforms.size(); j++)
					(this->*it->first->Technique->GlobalUniforms[j])(renderData);

				glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, (void*)(renderData.Batch->CommandOffset), renderData.Batch->DrawCount,0);

			}

			DrawSkybox(camera);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		for (CameraData* camera : cameras)
			glDrawTextureNV(camera->ColorTexture, 0, (GLfloat)camera->ScreenOffset.x, (GLfloat)camera->ScreenOffset.y, (GLfloat)camera->ScreenOffset.x + (GLfloat)camera->BaseCamera->GetViewport().x, (GLfloat)camera->ScreenOffset.y + (GLfloat)camera->BaseCamera->GetViewport().y, 0, 0, 0, 1, 1);

		CheckGLError();
	}

	void RES::Purge()
	{
		//Delete everything everywhere
		for (std::map<std::string, TextureData*>::iterator it = textures.begin(); it != textures.end(); it++)
		{
			glDeleteTextures(1, &it->second->Name);
			delete it->second;
		}
		textures.clear();

		for (std::map<std::string, MaterialData*>::iterator it = materials.begin(); it != materials.end(); it++)
		{
			delete it->second;
		}
		materials.clear();

		for (std::map<std::string, MeshData*>::iterator it = meshes.begin(); it != meshes.end(); it++)
		{
			delete it->second;
		}
		meshes.clear();

		for (std::map<std::string, TechniqueData*>::iterator it = techniques.begin(); it != techniques.end(); it++)
		{
			glDeleteProgram(it->second->Program);
			delete it->second;
		}
		techniques.clear();

		for (std::map<std::string, ActorData*>::iterator it = actors.begin(); it != actors.end(); it++)
		{
			delete it->second;
		}
		actors.clear();

		for (unsigned int i = 0; i < cameras.size(); i++)
		{
			glDeleteFramebuffers(1, &cameras[i]->Fbo);
			glDeleteTextures(1, &cameras[i]->ColorTexture);
			glDeleteTextures(1, &cameras[i]->DepthTexture);
			delete cameras[i];
		}
		cameras.clear();

		for (std::map<BatchData*, std::vector<InstanceData*>>::iterator it = batches.begin(); it != batches.end(); it++)
		{
			for (unsigned int i = 0; i < it->second.size(); i++)
			{
				delete it->second[i];
			}

			delete it->first;
		}
		batches.clear();

		if (skybox.Batch != nullptr)
		{
			delete skybox.Batch;
		}

		if (lightBuffer.ShadowBatch != nullptr)
		{
			delete lightBuffer.ShadowBatch;
		}

		if (skybox.Instance != nullptr)
		{
			delete skybox.Instance;
		}

		Log("Renderer: Purge completed");
	}

	void RES::InitStrToPfn()
	{
		//Build the string to function pointer map
		strToPfn.insert(std::make_pair("Position", &RES::SetupPositionAttribute));
		strToPfn.insert(std::make_pair("TexCoord", &RES::SetupTexCoordAttribute));
		strToPfn.insert(std::make_pair("Normal", &RES::SetupNormalAttribute));
		strToPfn.insert(std::make_pair("ModelMatrix", &RES::SetupModelMatrixUniform));
		strToPfn.insert(std::make_pair("ViewMatrix", &RES::SetupViewMatrixUniform));
		strToPfn.insert(std::make_pair("InverseViewMatrix", &RES::SetupInverseViewMatrixUniform));
		strToPfn.insert(std::make_pair("ProjMatrix", &RES::SetupProjMatrixUniform));
		strToPfn.insert(std::make_pair("CamPosition", &RES::SetupCamPositionUniform));
		strToPfn.insert(std::make_pair("CamTarget", &RES::SetupCamTargetUniform));
		strToPfn.insert(std::make_pair("CamUp", &RES::SetupCamUpUniform));
		strToPfn.insert(std::make_pair("Viewport", &RES::SetupViewportUniform));
		strToPfn.insert(std::make_pair("FieldOfView", &RES::SetupFieldOfViewUniform));
		strToPfn.insert(std::make_pair("Materials", &RES::SetupMaterialTexture));
		strToPfn.insert(std::make_pair("Diffuse", &RES::SetupDiffuseLayer));
		strToPfn.insert(std::make_pair("Specular", &RES::SetupSpecularLayer));
		strToPfn.insert(std::make_pair("Gloss", &RES::SetupGlossLayer));
		strToPfn.insert(std::make_pair("ModelMatrixBlock", &RES::SetupModelMatrixBlockBindingPoint));
		strToPfn.insert(std::make_pair("ModelMatrixBuffer", &RES::SetupModelMatrixBufferBinding));
		strToPfn.insert(std::make_pair("InstanceId", &RES::SetupInstanceId));
		strToPfn.insert(std::make_pair("MaterialLayerBlock", &RES::SetupMaterialLayerBlockBindingPoint));
		strToPfn.insert(std::make_pair("MaterialLayerBuffer", &RES::SetupMaterialLayerBufferBinding));
		strToPfn.insert(std::make_pair("LightBlock", &RES::SetupLightBlockBindingPoint));
		strToPfn.insert(std::make_pair("LightBuffer", &RES::SetupLightBufferBinding));
		strToPfn.insert(std::make_pair("AmbientLightColor", &RES::SetupAmbientLightColor));
		strToPfn.insert(std::make_pair("LightCount", &RES::SetupLightCount));
		strToPfn.insert(std::make_pair("ShadowMatrixBlock", &RES::SetupShadowMatrixBlockBindingPoint));
		strToPfn.insert(std::make_pair("ShadowMatrixBuffer", &RES::SetupShadowMatrixBufferBinding));
		strToPfn.insert(std::make_pair("Shadows", &RES::SetupShadowTexture));
	}

	void RES::UpdateModelMatrix()
	{
		for (std::map<BatchData*, std::vector<InstanceData*>>::iterator it = batches.begin(); it != batches.end(); it++)
		{
			BatchData* batch = it->first;
			std::vector<InstanceData*>& instances = it->second;

			for (unsigned int i = 0; i < instances.size(); i++)
			{
				mat4 modelMatrix = instances[i]->Instance->GetMatrix();
				unsigned int cpt = 0;

				for (unsigned int j = 0; j < 4; j++)
				{
					for (unsigned int k = 0; k < 4; k++)
					{
						batch->ModelMatrixPtr[(instances[i]->Id*16) + cpt] = modelMatrix[j][k];
						cpt++;
					}
				}
			}

			glBindBuffer(GL_SHADER_STORAGE_BUFFER, batch->ModelMatrixBuffer);
			glFlushMappedBufferRange(GL_SHADER_STORAGE_BUFFER, 0, instances.size() * 16 * sizeof(float));
		}
	}

	void RES::UpdateLights()
	{
		unsigned int offset = 0;
		for (std::set<Light*>::iterator it = lights.begin(); it != lights.end(); it++)
		{
			lightBuffer.LightPtr[offset].Position = (*it)->Position;
			lightBuffer.LightPtr[offset].Direction = (*it)->Direction;
			lightBuffer.LightPtr[offset].Color = (*it)->Color;
			lightBuffer.LightPtr[offset].Angle = (*it)->Angle;
			lightBuffer.LightPtr[offset].CosAngle = (*it)->CosAngle;
			lightBuffer.LightPtr[offset].Attenuation = (*it)->Attenuation;

			offset++;
		}
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, lightBuffer.LightBuffer);
		glFlushMappedBufferRange(GL_SHADER_STORAGE_BUFFER, 0, lights.size() * sizeof(Light));

		offset = 0;
		for (std::set<Light*>::iterator it = lights.begin(); it != lights.end(); it++)
		{
			static const mat4 scaleBiasMatrix = mat4(vec4(0.5f, 0.0f, 0.0f, 0.0f),
				vec4(0.0f, 0.5f, 0.0f, 0.0f),
				vec4(0.0f, 0.0f, 0.5f, 0.0f),
				vec4(0.5f, 0.5f, 0.5f, 1.0f));

			FixedCamera lightCamera(degrees((*it)->Angle), 0.1, 2000, uvec2(1024, 1024), (*it)->Position, (*it)->Position + (*it)->Direction, vec3(0.0, 1.0, 0.0));
			
			mat4 lightProjMatrix = glm::perspective(lightCamera.GetFieldOfView(), float(lightCamera.GetViewport().x) / float(lightCamera.GetViewport().y), lightCamera.GetNearPlaneDistance(), lightCamera.GetFarPlaneDistance());
			mat4 lightViewMatrix = glm::lookAt(lightCamera.GetPosition(), lightCamera.GetLookAt(), lightCamera.GetUp());

			mat4 shadowMatrix = scaleBiasMatrix * lightProjMatrix * lightViewMatrix;

			for (unsigned int j = 0; j < 4; j++)
			{
				for (unsigned int k = 0; k < 4; k++)
				{
					lightBuffer.ShadowMatrixPtr[offset++] = shadowMatrix[j][k];
				}
			}
		}

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, lightBuffer.ShadowMatrixBuffer);
		glFlushMappedBufferRange(GL_SHADER_STORAGE_BUFFER, 0, lights.size() * sizeof(Light));
	}

	void RES::DrawSkybox(CameraData* camera)
	{
		projMatrix = glm::perspective(camera->BaseCamera->GetFieldOfView(), float(camera->BaseCamera->GetViewport().x) / float(camera->BaseCamera->GetViewport().y), camera->BaseCamera->GetNearPlaneDistance(), camera->BaseCamera->GetFarPlaneDistance());
		viewMatrix = glm::lookAt(camera->BaseCamera->GetPosition(), camera->BaseCamera->GetLookAt(), camera->BaseCamera->GetUp());

		ActorData* data = skybox.Instance->Actor;

		skybox.Camera = camera;

		glUseProgram(skybox.Batch->Technique->Program);

		glBindVertexArray(skybox.Batch->Vao);

		for (unsigned int j = 0; j < skybox.Batch->Technique->GlobalUniforms.size(); j++)
		{
			(this->*skybox.Batch->Technique->GlobalUniforms[j])(skybox);
		}

		for (unsigned int j = 0; j < skybox.Batch->Technique->InstanceUniforms.size(); j++)
		{
			(this->*skybox.Batch->Technique->InstanceUniforms[j])(skybox);
		}

		for (unsigned int j = 0; j < skybox.Batch->Technique->CameraUniforms.size(); j++)
		{
			(this->*skybox.Batch->Technique->CameraUniforms[j])(skybox);
		}

		for (unsigned int j = 0; j < skybox.Batch->Technique->Textures.size(); j++)
		{
			(this->*skybox.Batch->Technique->Textures[j])(skybox);
		}

		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, (void*)(skybox.Batch->CommandOffset), skybox.Batch->DrawCount, 0);
	}

	void RES::SetAmbientLightColor(vec3 ambient)
	{
		this->ambient = ambient;
	}

	void RES::AddLight(Light* light)
	{
		if (lightBuffer.LightPtr == nullptr)
			lights.insert(light);
	}

	void RES::RemoveLight(Light* light)
	{
		lights.erase(light);
	}

	void RES::ShadowPass()
	{
		unsigned int cpt = 0;

		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(4.0f, 4.0f);

		for (std::set<Light*>::iterator it = lights.begin(); it != lights.end(); it++)
		{
			glBindFramebuffer(GL_FRAMEBUFFER, lightBuffer.ShadowFramebuffer[cpt]);
			
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glViewport(0, 0, 1024, 1024);

			Light* light = (*it);

			FixedCamera lightCamera(degrees(light->Angle),0.1,2000,uvec2(1024,1024),light->Position,light->Position+light->Direction,vec3(0.0,1.0,0.0));
			CameraData cameraData;
			cameraData.BaseCamera = &lightCamera;

			RenderData renderData;
			renderData.Camera = &cameraData;
			renderData.Batch = lightBuffer.ShadowBatch;

			projMatrix = glm::perspective(cameraData.BaseCamera->GetFieldOfView(), float(cameraData.BaseCamera->GetViewport().x) / float(cameraData.BaseCamera->GetViewport().y), cameraData.BaseCamera->GetNearPlaneDistance(), cameraData.BaseCamera->GetFarPlaneDistance());
			viewMatrix = glm::lookAt(cameraData.BaseCamera->GetPosition(), cameraData.BaseCamera->GetLookAt(), cameraData.BaseCamera->GetUp());


			glUseProgram(renderData.Batch->Technique->Program);

			glBindVertexArray(renderData.Batch->Vao);

			//Setup uniform
			for (unsigned int j = 0; j < renderData.Batch->Technique->CameraUniforms.size(); j++)
				(this->*renderData.Batch->Technique->CameraUniforms[j])(renderData);

			for (unsigned int j = 0; j < renderData.Batch->Technique->Textures.size(); j++)
				(this->*renderData.Batch->Technique->Textures[j])(renderData);

			for (unsigned int j = 0; j < renderData.Batch->Technique->GlobalUniforms.size(); j++)
				(this->*renderData.Batch->Technique->GlobalUniforms[j])(renderData);

			glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, (void*)(renderData.Batch->CommandOffset), renderData.Batch->DrawCount, 0);

			cpt++;
		}


		glDisable(GL_POLYGON_OFFSET_FILL);

		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		CheckGLError();
	}
}