#ifndef _RLIGHT_

#define _RLIGHT_


#include <glm/glm.hpp>

using namespace glm;

namespace Rarium
{
	struct Light
	{
		vec3 Position;
		vec3 Direction;
		vec3 Color;
		float Angle;
		float CosAngle;
		float Attenuation;

		Light(vec3 position, vec3 direction, vec3 color, float angle, float attenuation)
		{
			this->Position = position;
			this->Direction = direction;
			this->Color = color;
			this->Angle = angle;
			this->CosAngle = cos(angle);
			this->Attenuation = attenuation;
		}
	};

}

#endif