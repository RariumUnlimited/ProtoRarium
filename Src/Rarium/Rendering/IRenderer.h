#ifndef _RIRENDERER_

#define _RIRENDERER_

#include <Rarium/Core.h>
#include <Rarium/Engine/ActorInstance.h>
#include <Rarium/Engine/Camera.h>
#include <Rarium/Rendering/Light.h>

namespace Rarium
{
	/**
	*	Interface to implement an opengl renderer
	*/
	class IRenderer
	{
	public:
		/**
		*	Initialize the renderer
		*	@param width Width of the window
		*	@param height Height of the window
		*	@return True if the renderer successfully init
		*/
		virtual bool Init(unsigned int width, unsigned int height) = 0;
		/**
		*	Add a camera used to render on screen
		*	@param camera Camera used to renderer
		*	@param screenOffset Offset on screen where to display camera render
		*/
		virtual void AddCamera(Camera* camera, uvec2 screenOffset) = 0;
		/**
		*	Draw a frame
		*	@param delta Delta time between the last frame and now
		*/
		virtual void DrawFrame(float delta) = 0;
		/**
		*	Add an actor instance to the scene to be draw
		*	@param instance Actor instance to add to the scene
		*/
		virtual void AddActorInstance(ActorInstance* instance) = 0;
		/**
		*	Delete all data on the GPU
		*/
		virtual void Purge() = 0;
		/**
		*	Build all the data to be send to the gpu
		*/
		virtual void Build() = 0;
		/**
		*	Tell the renderer which skybox to use
		*	@param skybox Skybox to use
		*/
		virtual void SetSkybox(ActorInstance* skybox) = 0;
		/**
		*	Set the ambient light color for the scene
		*	@param ambient Ambient light color for the scene
		*/
		virtual void SetAmbientLightColor(vec3 ambient) = 0;
		/**
		*	Add a light to the scene
		*	@param light Light to add
		*/
		virtual void AddLight(Light* light) = 0;
		/**
		*	Remove a light from the scene
		*	@param light Light to remove
		*/
		virtual void RemoveLight(Light* light) = 0;
	};
}

#endif