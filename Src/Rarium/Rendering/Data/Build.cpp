#include <Rarium/Rendering/RES.h>
#include <Rarium/ResourceHandling/Catalog.h>

namespace Rarium
{
	void RES::BuildMesh()
	{
		std::vector<Mesh*> meshes;
		std::vector<MeshData*> meshesData;

		for (std::map<std::string, Mesh*>::iterator it = meshToBuild.begin(); it != meshToBuild.end(); it++)
		{
			meshes.push_back(it->second);
		}

		int totalVertex = 0;
		int totalIndices = 0;
		
		//First we build all the data about meshes
		for (unsigned int i = 0; i < meshes.size(); i++)
		{
			MeshData* data = new MeshData;
			data->BaseMesh = meshes[i];
			data->BaseVertex = totalVertex;
			data->BaseIndex = totalIndices;
			totalVertex += data->BaseMesh->GetVertexCount();
			totalIndices += data->BaseMesh->GetIndexCount();
			this->meshes.insert(std::make_pair(data->BaseMesh->Name(), data));
		}

		//Build and map the vertex and index buffer
		glGenBuffers(1, &meshBuffer.VertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, meshBuffer.VertexBuffer);
		glBufferStorage(GL_ARRAY_BUFFER, totalVertex * sizeof(VertexData), nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
		meshBuffer.VertexPtr = (VertexData*)glMapBufferRange(GL_ARRAY_BUFFER, 0, totalVertex * sizeof(VertexData), GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);

		glGenBuffers(1, &meshBuffer.IndexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshBuffer.IndexBuffer);
		glBufferStorage(GL_ELEMENT_ARRAY_BUFFER, totalIndices * sizeof(unsigned int), nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
		meshBuffer.IndexPtr = (unsigned int*)glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, totalIndices * sizeof(unsigned int), GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);

		//Transfert all data to the buffers
		unsigned int vertexOffset = 0;
		unsigned int indexOffset = 0;
		for (unsigned int i = 0; i < meshes.size(); i++)
		{
			Mesh* mesh = meshes[i];
			for (unsigned int j = 0; j < mesh->GetVertexCount(); j++)
			{
				meshBuffer.VertexPtr[vertexOffset++] = mesh->GetVertices()[j];
			}

			for (unsigned int j = 0; j < mesh->GetIndexCount(); j++)
			{
				meshBuffer.IndexPtr[indexOffset++] = mesh->GetIndexes()[j];
			}
		}

		glFlushMappedBufferRange(GL_ARRAY_BUFFER, 0, totalVertex * sizeof(VertexData));
		glFlushMappedBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, totalIndices * sizeof(unsigned int));

		CheckGLError();

		meshToBuild.clear();
	}

	GLuint RES::GetCommandOffset()
	{
		return commandToUpload.size() * sizeof(DrawElementsIndirectCommand);
	}

	void RES::Build()
	{
		BuildMesh();
		BuildSkybox();

		//Build the batch and get the technique for meshes
		BatchData* meshBatch = new BatchData;
		RegisterTechnique(Catalog::Get().RegisterResource<Technique>("Mesh"), meshBatch);


		meshBatch->Name = "MeshBatch";

		std::vector<InstanceData*> instances;

		std::vector<Material*> materialToBuild;


		bool buildModelMatrix = false;
		bool buildMaterialLayer = false;

		//Check if we have to build the model matrix and material layer buffer
		Property bufferBindingProp = meshBatch->Technique->BaseTechnique->GetProperty("BufferBinding");

		if (bufferBindingProp.GetName() != "")
		{
			for (std::string& str : bufferBindingProp.GetValues())
			{
				if (str == "ModelMatrixBlock")
				{
					buildModelMatrix = true;
				}
				if (str == "MaterialLayerBlock")
				{
					buildMaterialLayer = true;
				}
			}
		}

		//Create the storage for buffers if we need to
		if (buildModelMatrix)
		{
			glGenBuffers(1, &meshBatch->ModelMatrixBuffer);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, meshBatch->ModelMatrixBuffer);
			glBufferStorage(GL_SHADER_STORAGE_BUFFER, instanceToBuild.size() * 16 * sizeof(float), nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
			meshBatch->ModelMatrixPtr = (float*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, instanceToBuild.size() * 16 * sizeof(float), GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);
			CheckGLError();
		}

		if (buildMaterialLayer)
		{
			glGenBuffers(1, &meshBatch->MaterialLayerBuffer);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, meshBatch->MaterialLayerBuffer);
			glBufferStorage(GL_SHADER_STORAGE_BUFFER, instanceToBuild.size() * 3 * sizeof(int), nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
			meshBatch->MaterialLayerPtr = (int*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, instanceToBuild.size() * 3 * sizeof(int), GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);
			CheckGLError();
		}

		meshBatch->DrawCount = instanceToBuild.size();
		meshBatch->CommandOffset = GetCommandOffset();

		//Gather all materials used. TODO: Check unique material
		for (unsigned int i = 0; i < instanceToBuild.size(); i++)
		{
			ActorInstance* instance = instanceToBuild[i];
			materialToBuild.push_back(instance->GetActor()->GetMaterial());
		}

		//Build materials
		TextureData* texData = BuildMaterials(materialToBuild, meshBatch->Name);
		meshBatch->MaterialTexArray = texData;

		unsigned int offsetModelMatrix = 0;
		unsigned int offsetMaterialLayer = 0;

		//Create instances data
		for (unsigned int i = 0; i < instanceToBuild.size(); i++)
		{
			ActorInstance* instance = instanceToBuild[i];
			InstanceData* data = new InstanceData;
			data->Actor = RegisterActor(instance->GetActor(), meshBatch);
			data->Instance = instance;
			data->Id = i;

			//Create a new command. TODO: Instancing
			DrawElementsIndirectCommand cmd(data->Actor->Mesh->BaseMesh->GetIndexCount(), 1, data->Actor->Mesh->BaseIndex, data->Actor->Mesh->BaseVertex, 0);
			commandToUpload.push_back(cmd);

			//Update the model matrix and material layer buffers if it is needed
			if (buildModelMatrix)
			{
				mat4 modelMatrix = instance->GetMatrix();

				for (unsigned int j = 0; j < 4; j++)
				{
					for (unsigned int k = 0; k < 4; k++)
					{
						meshBatch->ModelMatrixPtr[offsetModelMatrix++] = modelMatrix[j][k];
					}
				}
			}

			if (buildMaterialLayer)
			{
				MaterialData* matData = data->Actor->Material;
				meshBatch->MaterialLayerPtr[offsetMaterialLayer++] = matData->DiffuseTextureLayer;
				meshBatch->MaterialLayerPtr[offsetMaterialLayer++] = matData->SpecularTextureLayer;
				meshBatch->MaterialLayerPtr[offsetMaterialLayer++] = matData->GlossTextureLayer;
			}

			instances.push_back(data);
		}

		if (buildModelMatrix)
		{
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, meshBatch->ModelMatrixBuffer);
			glFlushMappedBufferRange(GL_SHADER_STORAGE_BUFFER, 0, instanceToBuild.size() * 16 * sizeof(float));
		}

		if (buildMaterialLayer)
		{
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, meshBatch->MaterialLayerBuffer);
			glFlushMappedBufferRange(GL_SHADER_STORAGE_BUFFER, 0, instanceToBuild.size() * 3 * sizeof(int));
		}

		CheckGLError();

		//Build the batch VAO
		glGenVertexArrays(1, &meshBatch->Vao);
		glBindVertexArray(meshBatch->Vao);

		glBindBuffer(GL_ARRAY_BUFFER, meshBuffer.VertexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshBuffer.IndexBuffer);

		RenderData renderDataTemp;
		renderDataTemp.Batch = meshBatch;

		for (unsigned int i = 0; i < meshBatch->Technique->Attributes.size(); i++)
		{
			(this->*meshBatch->Technique->Attributes[i])(renderDataTemp);
		}

		glBindVertexArray(0);

		CheckGLError();

		batches.insert(std::make_pair(meshBatch, instances));

		instanceToBuild.clear();

		BuildCommandBuffer();
		BuildLights();
	}

	void RES::BuildCommandBuffer()
	{
		//Create buffer and upload commands
		glGenBuffers(1, &commandBuffer.CommandBuffer);
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, commandBuffer.CommandBuffer);
		glBufferStorage(GL_DRAW_INDIRECT_BUFFER, commandToUpload.size() * sizeof(DrawElementsIndirectCommand), nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
		commandBuffer.CommandPtr = (DrawElementsIndirectCommand*)glMapBufferRange(GL_DRAW_INDIRECT_BUFFER, 0, commandToUpload.size() * sizeof(DrawElementsIndirectCommand), GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);

		for (unsigned int i = 0; i < commandToUpload.size(); i++)
		{
			commandBuffer.CommandPtr[i] = commandToUpload[i];
		}

		glFlushMappedBufferRange(GL_DRAW_INDIRECT_BUFFER, 0, commandToUpload.size() * sizeof(DrawElementsIndirectCommand));

		CheckGLError();
		commandToUpload.clear();
	}

	TextureData* RES::BuildMaterials(std::vector<Material*>& materials, std::string batchName)
	{
		int materialCount = 0;

		gli::texture2D* firstTex = materials.front()->GetDiffuse()->GetData();

		GLuint tex;
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D_ARRAY, tex);

		glTexStorage3D(GL_TEXTURE_2D_ARRAY, firstTex->levels(), gli::internal_format(firstTex->format()), firstTex->dimensions().x, firstTex->dimensions().y, materials.size() * 3);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		CheckGLError();

		for (unsigned int i = 0; i < materials.size(); i++)
		{
			Material* material = materials[i];
			MaterialData* data = new MaterialData;
			data->BaseMaterial = material;

			data->DiffuseTextureLayer = materialCount++;
			data->SpecularTextureLayer = materialCount++;
			data->GlossTextureLayer = materialCount++;

			UploadTextureToArray(material->GetDiffuse(), data->DiffuseTextureLayer);
			UploadTextureToArray(material->GetSpecular(), data->SpecularTextureLayer);
			UploadTextureToArray(material->GetGloss(), data->GlossTextureLayer);

			this->materials.insert(std::make_pair(material->Name(), data));
		}

		TextureData* texData = new TextureData;
		texData->Name = tex;

		textures.insert(std::make_pair("TextureArrayMaterial" + batchName, texData));

		return texData;
	}

	void RES::UploadTextureToArray(Texture* texture, int layer)
	{
		gli::texture2D* tex = texture->GetData();

		if (gli::is_compressed(tex->format()))
		{
			for (gli::texture2D::size_type Level = 0; Level < tex->levels(); ++Level)
			{
				glCompressedTexSubImage3D(GL_TEXTURE_2D_ARRAY,
					static_cast<GLint>(Level), 0, 0, layer,
					static_cast<GLsizei>((*tex)[Level].dimensions().x),
					static_cast<GLsizei>((*tex)[Level].dimensions().y),
					1,
					static_cast<GLenum>(gli::external_format(tex->format())),
					static_cast<GLsizei>((*tex)[Level].size()),
					(*tex)[Level].data());
			}
		}
		else
		{
			for (gli::texture2D::size_type Level = 0; Level < tex->levels(); ++Level)
			{
				glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
					static_cast<GLint>(Level), 0, 0, layer,
					static_cast<GLsizei>((*tex)[Level].dimensions().x),
					static_cast<GLsizei>((*tex)[Level].dimensions().y),
					1,
					static_cast<GLenum>(gli::external_format(tex->format())),
					static_cast<GLenum>(gli::type_format(tex->format())),
					(*tex)[Level].data());
			}
		}

		CheckGLError();
	}

	void RES::BuildSkybox()
	{
		this->skybox.Batch = new BatchData;
		this->skybox.Batch->Technique = RegisterTechnique(Catalog::Get().RegisterResource<Technique>("Skybox"), this->skybox.Batch);
		
		//Create the batch vao
		glGenVertexArrays(1, &this->skybox.Batch->Vao);
		glBindVertexArray(this->skybox.Batch->Vao);

		glBindBuffer(GL_ARRAY_BUFFER, meshBuffer.VertexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshBuffer.IndexBuffer);


		for (unsigned int i = 0; i < this->skybox.Batch->Technique->Attributes.size(); i++)
		{
			(this->*this->skybox.Batch->Technique->Attributes[i])(this->skybox);
		}

		glBindVertexArray(0);

		GLuint tex;
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D_ARRAY, tex);

		gli::texture2D* firstTex = skyboxToBuild->GetActor()->GetMaterial()->GetDiffuse()->GetData();

		glTexStorage3D(GL_TEXTURE_2D_ARRAY, firstTex->levels(), gli::internal_format(firstTex->format()), firstTex->dimensions().x, firstTex->dimensions().y, 1);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		UploadTextureToArray(skyboxToBuild->GetActor()->GetMaterial()->GetDiffuse(), 0);

		MaterialData* skyboxMat = new MaterialData;
		skyboxMat->DiffuseTextureLayer = 0;
		skyboxMat->BaseMaterial = skyboxToBuild->GetActor()->GetMaterial();

		TextureData* skyboxTex = new TextureData;
		skyboxTex->BaseTexture = skyboxToBuild->GetActor()->GetMaterial()->GetDiffuse();
		skyboxTex->Name = tex;

		textures.insert(std::make_pair("TextureArrayMaterialSkybox", skyboxTex));
		materials.insert(std::make_pair(skyboxToBuild->GetActor()->GetMaterial()->Name(), skyboxMat));

		this->skybox.Batch->MaterialTexArray = skyboxTex;

		InstanceData* data = new InstanceData;
		data->Actor = RegisterActor(skyboxToBuild->GetActor(), this->skybox.Batch);
		data->Instance = skyboxToBuild;
		this->skybox.Instance = data;

		skybox.Batch->CommandOffset = GetCommandOffset();
		skybox.Batch->DrawCount = 1;
		DrawElementsIndirectCommand skyboxCmd(skybox.Instance->Actor->Mesh->BaseMesh->GetIndexCount(), 1, skybox.Instance->Actor->Mesh->BaseIndex, skybox.Instance->Actor->Mesh->BaseVertex, 0);
		commandToUpload.push_back(skyboxCmd);
	}

	void RES::BuildLights()
	{
		if (lights.size() > 0)
		{
			//Create buffer and upload lights
			glGenBuffers(1, &lightBuffer.LightBuffer);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, lightBuffer.LightBuffer);
			glBufferStorage(GL_SHADER_STORAGE_BUFFER, lights.size() * sizeof(LightData), nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);

			lightBuffer.LightPtr = (LightData*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, lights.size() * sizeof(LightData), GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);

			unsigned int offset = 0;
			for (std::set<Light*>::iterator it = lights.begin(); it != lights.end(); it++)
			{
				lightBuffer.LightPtr[offset].Position = (*it)->Position;
				lightBuffer.LightPtr[offset].Direction = (*it)->Direction;
				lightBuffer.LightPtr[offset].Color = (*it)->Color;
				lightBuffer.LightPtr[offset].Angle = (*it)->Angle;
				lightBuffer.LightPtr[offset].CosAngle = (*it)->CosAngle;
				lightBuffer.LightPtr[offset].Attenuation = (*it)->Attenuation;

				offset++;
			}

			glFlushMappedBufferRange(GL_SHADER_STORAGE_BUFFER, 0, lights.size() * sizeof(LightData));

			glGenBuffers(1, &lightBuffer.ShadowMatrixBuffer);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, lightBuffer.ShadowMatrixBuffer);
			glBufferStorage(GL_SHADER_STORAGE_BUFFER, lights.size() * sizeof(float) * 16, nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);

			lightBuffer.ShadowMatrixPtr = (float*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, lights.size() * sizeof(float) * 16, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);

			glGenTextures(1, &lightBuffer.ShadowMapTexArray);
			glBindTexture(GL_TEXTURE_2D_ARRAY, lightBuffer.ShadowMapTexArray);
			glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_DEPTH_COMPONENT32F, 1024, 1024, lights.size());
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

			unsigned int cpt = 0;

			for (std::set<Light*>::iterator it = lights.begin(); it != lights.end(); it++)
			{
				GLuint framebuffer;
				glGenFramebuffers(1, &framebuffer);
				glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
				glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, lightBuffer.ShadowMapTexArray, 0, cpt);

				glDrawBuffer(GL_NONE);
				if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
					throw Exception("Framebuffer not complete");

				lightBuffer.ShadowFramebuffer.push_back(framebuffer);
				cpt++;
			}

			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			lightBuffer.ShadowBatch = new BatchData;

			for (std::map<BatchData*, std::vector<InstanceData*>>::iterator it = batches.begin(); it != batches.end(); it++)
			{
				if (it->first->Name == "MeshBatch")
				{
					(*lightBuffer.ShadowBatch) = (*it->first);
					break;
				}
			}

			lightBuffer.ShadowBatch->Technique = RegisterTechnique(Catalog::Get().RegisterResource<Technique>("ShadowMap"), lightBuffer.ShadowBatch);

			CheckGLError();
		}
	}
}