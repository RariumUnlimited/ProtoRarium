#include <Rarium/Rendering/RES.h>
#include <Rarium/ResourceHandling/Catalog.h>


namespace Rarium
{
	void RES::AddActorInstance(ActorInstance* instance)
	{
		meshToBuild.insert(std::make_pair(instance->GetActor()->GetMesh()->Name(), instance->GetActor()->GetMesh()));
		instanceToBuild.push_back(instance);
	}

	void RES::SetSkybox(ActorInstance* skybox)
	{
		this->skyboxToBuild = skybox;
		meshToBuild.insert(std::make_pair(skybox->GetActor()->GetMesh()->Name(), skybox->GetActor()->GetMesh()));
	}

	ActorData* RES::RegisterActor(Actor* actor, BatchData* batch)
	{
		std::map<std::string,ActorData*>::iterator it = actors.find(actor->Name());
		if(it == actors.end())
		{
			ActorData* data = new ActorData;
			data->BaseActor = actor;
			//Get the material, mesh and technique of the actor
			data->Material = materials[actor->GetMaterial()->Name()];
			data->Mesh = meshes[actor->GetMesh()->Name()];

			actors.insert(std::make_pair(actor->Name(),data));

			return data;
		}
		else
			return it->second;
	}


	TechniqueData* RES::RegisterTechnique(Technique* technique, BatchData* batch)
	{
		std::map<std::string,TechniqueData*>::iterator it = techniques.find(technique->Name());
		if(it == techniques.end())
		{
			TechniqueData* data = new TechniqueData;
			data->BaseTechnique = technique;
			//Load shaders
			GLuint vs = LoadShaderFromSource(GL_VERTEX_SHADER,technique->GetVertexShaderSource());
			GLuint fs = LoadShaderFromSource(GL_FRAGMENT_SHADER,technique->GetFragmentShaderSource());

			//Create and link program
			data->Program = glCreateProgram();

			glAttachShader(data->Program, vs);
			glAttachShader(data->Program, fs);

			glLinkProgram(data->Program);

			//Check linking status
			GLint status;

			glGetProgramiv(data->Program, GL_LINK_STATUS, &status);

			if (status != GL_TRUE)
			{
				GLchar log[2048];
				GLsizei size;
				glGetProgramInfoLog(data->Program, 2048, &size, log);
				
				Log(ToString(log));
				throw Exception("Error while linking program :");
			}


			glDeleteShader(vs);
			glDeleteShader(fs);

			techniques.insert(std::make_pair(technique->Name(),data));
			technique->Release();

			batch->Technique = data;

			CheckGLError();

			Property attributeProp = technique->GetProperty("Attribute");
			if(attributeProp.GetName() != "")
			{
				for(std::string& str : attributeProp.GetValues())
				{
					data->Attributes.push_back(strToPfn[str]);
					data->Locations.insert(std::make_pair(str,glGetAttribLocation(data->Program,str.c_str())));
				}
			}

			CheckGLError();

			Property globalUniformProp = technique->GetProperty("GlobalUniform");
			if (globalUniformProp.GetName() != "")
			{
				for (std::string& str : globalUniformProp.GetValues())
				{
					data->GlobalUniforms.push_back(strToPfn[str]);
					data->Locations.insert(std::make_pair(str,glGetUniformLocation(data->Program,str.c_str())));
				}
			}

			CheckGLError();

			Property instanceUniformProp = technique->GetProperty("InstanceUniform");
			if (instanceUniformProp.GetName() != "")
			{
				for (std::string& str : instanceUniformProp.GetValues())
				{
					data->InstanceUniforms.push_back(strToPfn[str]);
					data->Locations.insert(std::make_pair(str, glGetUniformLocation(data->Program, str.c_str())));
				}
			}

			CheckGLError();

			Property cameraUniformProp = technique->GetProperty("CameraUniform");
			if (cameraUniformProp.GetName() != "")
			{
				for (std::string& str : cameraUniformProp.GetValues())
				{
					data->CameraUniforms.push_back(strToPfn[str]);
					data->Locations.insert(std::make_pair(str, glGetUniformLocation(data->Program, str.c_str())));
				}
			}

			CheckGLError();

			Property textureProp = technique->GetProperty("Texture");
			if(textureProp.GetName() != "")
			{
				for(std::string& str : textureProp.GetValues())
				{
					data->Textures.push_back(strToPfn[str]);
					data->Locations.insert(std::make_pair(str,glGetUniformLocation(data->Program,str.c_str())));
				}
			}

			CheckGLError();

			Property bufferBindingProp = technique->GetProperty("BufferBinding");
			if (bufferBindingProp.GetName() != "")
			{
				RenderData renderData;
				renderData.Batch = batch;

				for (std::string& str : bufferBindingProp.GetValues())
				{
					(this->*strToPfn[str])(renderData);
				}
			}

			CheckGLError();

			return data;
		}
		else
			return it->second;
	}

	void RES::AddCamera(Camera* camera, uvec2 screenOffset)
	{
		CameraData* data = new CameraData;
		data->BaseCamera = camera;
		data->ScreenOffset = screenOffset;
		cameras.push_back(data);

		glGenFramebuffers(1, &data->Fbo);
		glBindFramebuffer(GL_FRAMEBUFFER, data->Fbo);

		glGenTextures(1, &data->ColorTexture);
		glBindTexture(GL_TEXTURE_2D, data->ColorTexture);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, camera->GetViewport().x, camera->GetViewport().y);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenTextures(1, &data->DepthTexture);
		glBindTexture(GL_TEXTURE_2D, data->DepthTexture);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, camera->GetViewport().x, camera->GetViewport().y);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, data->ColorTexture, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, data->DepthTexture, 0);

		GLenum framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);

		if (framebufferStatus != GL_FRAMEBUFFER_COMPLETE)
		{
			throw Exception("Framebuffer not complete : " + framebufferStatus);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		CheckGLError();
	}

	GLuint RES::LoadShaderFromSource(GLenum type, char* source)
	{
		GLuint shader = glCreateShader(type);

		const char* shaderSrc = source;

		glShaderSource(shader, 1, &shaderSrc, NULL);

		glCompileShader(shader);

		GLint compileStatus = 0;
		glGetShaderiv(shader,GL_COMPILE_STATUS,&compileStatus);

		if(compileStatus != GL_TRUE)
		{
			GLint infoLogSize = 0;
			glGetShaderiv(shader,GL_INFO_LOG_LENGTH,&infoLogSize);
			GLchar* log = new GLchar[infoLogSize];
			GLint logSize = 0;
			glGetShaderInfoLog(shader,infoLogSize,&logSize,log);

			Log("Shader : ");
			Log(source);
			Log("Compilation error : ");

			Log(log);

			delete[] log;

			throw Exception("Shader compilation error.");
		}

		CheckGLError();

		return shader;
	}

}