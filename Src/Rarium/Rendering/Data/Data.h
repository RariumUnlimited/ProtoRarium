#ifndef _RACTORDATA_

#define _RACTORDATA_

#include <GL/glew.h>
#include <Rarium/Core.h>
#include <Rarium/Resources/Actor.h>
#include <Rarium/Resources/Cubemap.h>

#define ModelMatrixBlockBindingPoint 0
#define MaterialLayerBlockBindingPoint 1
#define LightBlockBindingPoint 2
#define ShadowMatrixBlockBindingPoint 3

namespace Rarium
{
	class RES;

	/**
	*	Data about a texture send to the gpu
	*/
	struct TextureData
	{
		Texture* BaseTexture;///< Base texture resource
		GLuint Name;///< Name of the texture

		/**
		*	Build a default texture
		*/
		TextureData()
		{
			Name = 0;
			BaseTexture = nullptr;
		}
	};

	/**
	*	Data about a material send to the gpu
	*/
	struct MaterialData
	{
		Material* BaseMaterial;///< Base material resource
		int DiffuseTextureLayer;///< Index of the diffuse texture in the material texture array
		int GlossTextureLayer;///< Index of the gloss texture in the material texture array
		int SpecularTextureLayer;///< Index of the specular texture in the material texture array

		/**
		*	Build a default material
		*/
		MaterialData()
		{
			BaseMaterial = nullptr;
			DiffuseTextureLayer = -1;
			GlossTextureLayer = -1;
			SpecularTextureLayer = -1;
		}
	};

	/**
	*	Data about a mesh send to the gpu
	*/
	struct MeshData
	{
		Mesh* BaseMesh;///< Base mesh resource
		int BaseVertex;///< The number to add to the indexes of this mesh
		int BaseIndex;///< Offset in the index buffer of this mesh indexes

		/**
		*	Build a default mesh
		*/
		MeshData()
		{
			BaseMesh = nullptr;
			BaseVertex = 0;
			BaseIndex = 0;
		}
	};

	struct InstanceData;
	struct BatchData;

	/**
	*	Data about a camera used to render the scene
	*/
	struct CameraData
	{
		Camera* BaseCamera;///< Base camera
		uvec2 ScreenOffset;///< Offset on the screen where to display this camera final render
		GLuint Fbo;///< Name of the fbo associated with this camera
		GLuint ColorTexture;///< Name of the color texture of this camera
		GLuint DepthTexture;///< Name of the depth texture of this camera
	};

	/**
	*	Data about the current rendering state
	*/
	struct RenderData
	{
		InstanceData* Instance;///< Instance that is going to be drawn
		BatchData* Batch;///< Batch being currently processed
		CameraData* Camera;///< Camera currently used to render
	};

	/**
	*	Represent a technique can be use for rendering
	*/
	struct TechniqueData
	{
		Technique* BaseTechnique;///< Base technique resource
		GLuint Program;///< Program name of the technique
		std::map<std::string,GLint> Locations;///< Locations of all uniforms/attributes/textures needed by the technique
		std::vector<void (RES::*)(RenderData)> Attributes;///< Attributes needed to set for this technique
		std::vector<void (RES::*)(RenderData)> InstanceUniforms;///< Uniforms needed to set for this technique for every instance drawn (ex: material)
		std::vector<void (RES::*)(RenderData)> GlobalUniforms;///< Uniforms needed to set for this technique that do not depend on an instance (ex : Light uniforms)
		std::vector<void (RES::*)(RenderData)> CameraUniforms;///< Uniforms needed to set for this technique that depends on the camera (ex : View matrix)
		std::vector<void (RES::*)(RenderData)> Textures;///< Textures needed to set for this technique

		/**
		*	Build a default technique
		*/
		TechniqueData()
		{
			BaseTechnique = nullptr;
			Program = 0;
		}
	};

	/**
	*	Reprenset an actor that can be render
	*/
	struct ActorData
	{
		Actor* BaseActor;///< Base actor resource
		MaterialData* Material;///< Data for the actor material
		MeshData* Mesh;///< Data for the actor mesh

		/**
		*	Build a default actor
		*/
		ActorData()
		{
			BaseActor = nullptr;
			Material = nullptr;
			Mesh = nullptr;
		}
	};

	/**
	*	Data about an instance of the scene
	*/
	struct InstanceData
	{
		ActorData* Actor;///< Data about the actor of this instance
		ActorInstance* Instance;///< Base instance
		unsigned int Id;///< Id of this instance

		/**
		*	Build a default instance
		*/
		InstanceData()
		{
			Actor = nullptr;
			Instance = nullptr;
			Id = 0;
		}
	};

	/**
	*	Data about a batch of instance
	*/
	struct BatchData
	{
		TechniqueData* Technique;///< Technique used to render the instances
		TextureData* MaterialTexArray;///< Texture array containing all material of all the instances
		std::string Name;///< Name of the batch
		GLuint ModelMatrixBuffer;///< Buffer containing all of the model matrix of the instances
		float* ModelMatrixPtr;///< Persistent map of model matrix buffer
		GLuint MaterialLayerBuffer;///< Buffer containing all of material index layer of the instances
		int* MaterialLayerPtr;///< Persitent map of material layer buffer
		GLuint Vao;///< Vao of this batch
		GLuint DrawCount;///< How many indirect command we have for this batch
		GLuint CommandOffset;///< Offset of this batch's commands in the command buffer

		/**
		*	Build a default batch
		*/
		BatchData()
		{
			Technique = nullptr;
			MaterialTexArray = nullptr;
			MaterialLayerPtr = nullptr;
			ModelMatrixPtr = nullptr;
			MaterialLayerBuffer = ModelMatrixBuffer = Vao = DrawCount = CommandOffset = 0;
		}

		/**
		*	Compare a batch with another
		*	@param other Other batch to compare to
		*	@return true If this batch technique pointer value is less than the other
		*/
		inline bool operator<(BatchData& other)
		{
			return this->Technique < other.Technique;
		}
	};

	/**
	*	Data about buffer that store meshes
	*/
	struct MeshBufferData
	{
		GLuint VertexBuffer;///< Buffer containing all the vertex
		VertexData* VertexPtr;///< Persitent map of the vertex buffer
		GLuint IndexBuffer;///< Buffer containing all the index
		unsigned int* IndexPtr;///< Persitent map of the index buffer

		/**
		*	Build a default mesh buffer data
		*/
		MeshBufferData()
		{
			VertexBuffer = 0;
			IndexBuffer = 0;
			VertexPtr = nullptr;
			IndexPtr = nullptr;
		}
	};

	/**
	*	Indirect command, see : http://www.opengl.org/wiki/GlMultiDrawElementsIndirect
	*/
	struct DrawElementsIndirectCommand{
		GLuint  Count;
		GLuint  InstanceCount;
		GLuint  FirstIndex;
		GLuint  BaseVertex;
		GLuint  BaseInstance;

		/**
		*	Create a default command
		*/
		DrawElementsIndirectCommand(GLuint Count = 0, GLuint InstanceCount = 0, GLuint FirstIndex = 0, GLuint BaseVertex = 0, GLuint BaseInstance = 0)
		{
			this->Count = Count;
			this->InstanceCount = InstanceCount;
			this->FirstIndex = FirstIndex;
			this->BaseVertex = BaseVertex;
			this->BaseInstance = BaseInstance;
		}
	};

	/**
	*	Data about a buffer that store indirect command
	*/
	struct IndirectCommandBufferData
	{
		GLuint CommandBuffer;///< Buffer containing the commands
		DrawElementsIndirectCommand* CommandPtr;///< Persistent map of the buffer command

		IndirectCommandBufferData()
		{
			CommandBuffer = 0;
			CommandPtr = nullptr;
		}
	};

	struct LightData
	{
		vec3 Position;
		float Angle;
		vec3 Direction;
		float CosAngle;
		vec3 Color;
		float Attenuation;
	};

	/**
	*	Data about a buffer that store lights data
	*/
	struct LightBufferData
	{
		GLuint LightBuffer;///< Buffer containing the lights
		LightData* LightPtr;///< Persistent map of the lights buffer
		GLuint ShadowMapTexArray;///< Texture array containing shadow map for each light
		std::vector<GLuint> ShadowFramebuffer;///< Framebuffers used when generating shadow map
		BatchData* ShadowBatch;///< Batch used when rendering shadow map
		GLuint ShadowMatrixBuffer;///< Buffer containing the shadow matrix for all the light
		float* ShadowMatrixPtr;///< Persitent map of the shadow matrix buffer

		LightBufferData()
		{
			LightBuffer = 0;
			LightPtr = nullptr;
			ShadowMapTexArray = 0;
			ShadowBatch = nullptr;
		}
	};
}

#endif