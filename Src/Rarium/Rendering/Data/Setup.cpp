#include <Rarium/Rendering/RES.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>

namespace Rarium
{
	void RES::SetupPositionAttribute(RenderData data)
	{
		glVertexAttribPointer(data.Batch->Technique->Locations["Position"], 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void *)offsetof(VertexData, VertexData::Position));
		glEnableVertexAttribArray(data.Batch->Technique->Locations["Position"]);
	}

	void RES::SetupTexCoordAttribute(RenderData data)
	{
		glVertexAttribPointer(data.Batch->Technique->Locations["TexCoord"], 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void *)offsetof(VertexData, VertexData::TexCoord));
		glEnableVertexAttribArray(data.Batch->Technique->Locations["TexCoord"]);
	}

	void RES::SetupNormalAttribute(RenderData data)
	{
		glVertexAttribPointer(data.Batch->Technique->Locations["Normal"], 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void *)offsetof(VertexData, VertexData::Normal));
		glEnableVertexAttribArray(data.Batch->Technique->Locations["Normal"]);
	}

	void RES::SetupModelMatrixUniform(RenderData data)
	{
		glUniformMatrix4fv(data.Batch->Technique->Locations["ModelMatrix"], 1, GL_FALSE, glm::value_ptr(data.Instance->Instance->GetMatrix()));
	}

	void RES::SetupViewMatrixUniform(RenderData data)
	{
		glUniformMatrix4fv(data.Batch->Technique->Locations["ViewMatrix"], 1, GL_FALSE, glm::value_ptr(viewMatrix));
	}

	void RES::SetupProjMatrixUniform(RenderData data)
	{
		glUniformMatrix4fv(data.Batch->Technique->Locations["ProjMatrix"], 1, GL_FALSE, glm::value_ptr(projMatrix));
	}


	void RES::SetupInverseViewMatrixUniform(RenderData data)
	{
		glUniformMatrix4fv(data.Batch->Technique->Locations["InverseViewMatrix"], 1, GL_FALSE, glm::value_ptr(inverse(viewMatrix)));
	}

	void RES::SetupCamPositionUniform(RenderData data)
	{
		glUniform3fv(data.Batch->Technique->Locations["CamPostion"], 1, glm::value_ptr(data.Camera->BaseCamera->GetPosition()));
	}

	void RES::SetupCamTargetUniform(RenderData data)
	{
		glUniform3fv(data.Batch->Technique->Locations["CamTarget"], 1, glm::value_ptr(data.Camera->BaseCamera->GetLookAt()));
	}

	void RES::SetupCamUpUniform(RenderData data)
	{
		glUniform3fv(data.Batch->Technique->Locations["CamUp"], 1, glm::value_ptr(data.Camera->BaseCamera->GetUp()));
	}

	void RES::SetupViewportUniform(RenderData data)
	{
		vec2 viewport(width, height);
		glUniform2f(data.Batch->Technique->Locations["Viewport"], viewport.x, viewport.y);
	}

	void RES::SetupFieldOfViewUniform(RenderData data)
	{
		glUniform1f(data.Batch->Technique->Locations["FieldOfView"], radians(data.Camera->BaseCamera->GetFieldOfView()));
	}

	void RES::SetupMaterialTexture(RenderData data)
	{
		glUniform1i(data.Batch->Technique->Locations["Materials"], 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D_ARRAY, data.Batch->MaterialTexArray->Name);
	}

	void RES::SetupDiffuseLayer(RenderData data)
	{
		glUniform1i(data.Batch->Technique->Locations["Diffuse"], data.Instance->Actor->Material->DiffuseTextureLayer);
	}

	void RES::SetupSpecularLayer(RenderData data)
	{
		glUniform1i(data.Batch->Technique->Locations["Specular"], data.Instance->Actor->Material->DiffuseTextureLayer);
	}

	void RES::SetupGlossLayer(RenderData data)
	{
		glUniform1i(data.Batch->Technique->Locations["Gloss"], data.Instance->Actor->Material->DiffuseTextureLayer);
	}

	void RES::SetupModelMatrixBlockBindingPoint(RenderData data)
	{
		GLuint modelMatrixBlockIndex = glGetProgramResourceIndex(data.Batch->Technique->Program, GL_SHADER_STORAGE_BLOCK, "ModelMatrixBlock");
		if (modelMatrixBlockIndex != GL_INVALID_INDEX)
			glShaderStorageBlockBinding(data.Batch->Technique->Program, modelMatrixBlockIndex, ModelMatrixBlockBindingPoint);
	}

	void RES::SetupModelMatrixBufferBinding(RenderData data)
	{
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, ModelMatrixBlockBindingPoint, data.Batch->ModelMatrixBuffer);
	}

	void RES::SetupInstanceId(RenderData data)
	{
		glUniform1ui(data.Batch->Technique->Locations["InstanceId"], data.Instance->Id);
	}

	void RES::SetupMaterialLayerBlockBindingPoint(RenderData data)
	{
		GLuint materialLayerBlockIndex = glGetProgramResourceIndex(data.Batch->Technique->Program, GL_SHADER_STORAGE_BLOCK, "MaterialLayerBlock");
		if (materialLayerBlockIndex != GL_INVALID_INDEX)
			glShaderStorageBlockBinding(data.Batch->Technique->Program, materialLayerBlockIndex, MaterialLayerBlockBindingPoint);
	}

	void RES::SetupMaterialLayerBufferBinding(RenderData data)
	{
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, MaterialLayerBlockBindingPoint, data.Batch->MaterialLayerBuffer);
	}

	void RES::SetupLightBlockBindingPoint(RenderData data)
	{
		GLuint lightBlockIndex = glGetProgramResourceIndex(data.Batch->Technique->Program, GL_SHADER_STORAGE_BLOCK, "LightBlock");
		if (lightBlockIndex != GL_INVALID_INDEX)
			glShaderStorageBlockBinding(data.Batch->Technique->Program, lightBlockIndex, LightBlockBindingPoint);
	}

	void RES::SetupLightBufferBinding(RenderData data)
	{
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, LightBlockBindingPoint, lightBuffer.LightBuffer);
	}

	void RES::SetupAmbientLightColor(RenderData data)
	{
		glUniform3f(data.Batch->Technique->Locations["AmbientLightColor"], ambient.x, ambient.y, ambient.z);
	}

	void RES::SetupLightCount(RenderData data)
	{
		glUniform1ui(data.Batch->Technique->Locations["LightCount"], lights.size());
	}

	void RES::SetupShadowMatrixBlockBindingPoint(RenderData data)
	{
		GLuint shadowMatrixBlockIndex = glGetProgramResourceIndex(data.Batch->Technique->Program, GL_SHADER_STORAGE_BLOCK, "ShadowMatrixBlock");
		if (shadowMatrixBlockIndex != GL_INVALID_INDEX)
			glShaderStorageBlockBinding(data.Batch->Technique->Program, shadowMatrixBlockIndex, ShadowMatrixBlockBindingPoint);
	}

	void RES::SetupShadowMatrixBufferBinding(RenderData data)
	{
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, ShadowMatrixBlockBindingPoint, lightBuffer.ShadowMatrixBuffer);
	}

	void RES::SetupShadowTexture(RenderData data)
	{
		glUniform1i(data.Batch->Technique->Locations["Shadows"], 1);
		glActiveTexture(GL_TEXTURE0+1);
		glBindTexture(GL_TEXTURE_2D_ARRAY, lightBuffer.ShadowMapTexArray);
	}
}