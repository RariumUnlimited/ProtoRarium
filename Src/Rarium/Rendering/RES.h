#ifndef _RRES_

#define _RRES_

#include <Rarium/Core.h>
#include <Rarium/Rendering/IRenderer.h>
#include <Rarium/Resources/Texture.h>
#include <Rarium/Rendering/Data/Data.h>
#include <queue>

namespace Rarium
{
	/**
	*	Renderer
	*/	
	class RES : public IRenderer
	{
	public:
		/**
		*	Initialize the renderer
		*	@param width Width of the window
		*	@param height Height of the window
		*	@return True if the renderer successfully init
		*/
		virtual bool Init(unsigned int width, unsigned int height);
		/**
		*	Build all the data to be send to the gpu
		*/
		virtual void Build();
		/**
		*	Add a camera used to render on screen
		*	@param camera Camera used to renderer
		*	@param screenOffset Offset on screen where to display camera render
		*/
		virtual void AddCamera(Camera* camera,uvec2 screenOffset);
		/**
		*	Draw a frame
		*	@param delta Delta time between the last frame and now
		*/
		virtual void DrawFrame(float delta);
		/**
		*	Add an actor instance to the scene to be draw
		*	@param instance Actor instance to add to the scene
		*/
		virtual void AddActorInstance(ActorInstance* instance);
		/**
		*	Delete all data on the GPU
		*/
		virtual void Purge();
		/**
		*	Tell the renderer which skybox to use
		*	@param skybox Skybox to use
		*/
		virtual void SetSkybox(ActorInstance* skybox);
		/**
		*	Set the ambient light color for the scene
		*	@param ambient Ambient light color for the scene
		*/
		virtual void SetAmbientLightColor(vec3 ambient);
		/**
		*	Add a light to the scene
		*	@param light Light to add
		*/
		virtual void AddLight(Light* light);
		/**
		*	Remove a light from the scene
		*	@param light Light to remove
		*/
		virtual void RemoveLight(Light* light);

		/**
		*	Load a shader from its sources
		*	@param type Type of the shader, either GL_VERTEX_SHADER or GL_FRAGMENT_SHADER
		*	@param source Source code of the shader
		*	@return Name of the shader created
		*/
		static GLuint LoadShaderFromSource(GLenum type, char* source);

	protected:
		unsigned int width;///< Screen width
		unsigned int height;///< Screen height
		std::vector<CameraData*> cameras;///< Camera that render the scene each frame
		std::map<std::string, void (RES::*)(RenderData)> strToPfn;///< Map where we store for each attribute/uniform a pointer to the function to set it up
		mat4 viewMatrix;///< View matrix of the camera
		mat4 projMatrix;///< Proj matrix of the camera
		RenderData skybox;///< The render data used when drawing the skybox
		ActorInstance* skyboxToBuild;///< The instance corresponding to our skybox
		vec3 ambient;///< Ambient light color of the scene

		std::vector<ActorInstance*> instanceToBuild;//!< List of all instance that will be in the scene and that needs to be build
		std::map<std::string, Mesh*> meshToBuild;//!< List of all mesh to build
		std::vector<DrawElementsIndirectCommand> commandToUpload;//!< List of all indirect command to upload

		std::map<std::string,MaterialData*> materials;///< List of all materials registered on the gpu
		std::map<std::string,MeshData*> meshes;///< List of all meshes registered on the gpu
		std::map<std::string,TechniqueData*> techniques;///< List of all techniques registered on the gpu
		std::map<std::string,ActorData*> actors;///< List of all actors registered on the gpu
		std::map<std::string,TextureData*> textures;///< List of all textures registered on the gpu
		std::map<BatchData*, std::vector<InstanceData*>> batches;//!< List of all instance batches
		MeshBufferData meshBuffer;///< Data about the buffer where we stored all the meshes
		IndirectCommandBufferData commandBuffer;///< Data about the buffer where we stored all our indirect command
		LightBufferData lightBuffer;///< Data about the buffer where we store all our lights
		std::set<Light*> lights;

		/**
		*	Render the scene
		*/
		void ScenePass();
		/**
		*	Generate the shadow map
		*/
		void ShadowPass();
		/**
		*	Draw the skybox
		*/
		void DrawSkybox(CameraData* camera);

		/**
		*	Initialize the strToPfn map
		*/
		void InitStrToPfn();

		/**
		*	Register an actor resource on the gpu
		*	@param actor Actor to register
		*/
		ActorData* RegisterActor(Actor* actor, BatchData* batch);
		/**
		*	Register a technique resource on the gpu
		*	@param technique Technique to register
		*/
		TechniqueData* RegisterTechnique(Technique* technique, BatchData* batch);
		/**
		*	Build a set of material in a texture array
		*	@param materials Set of material to build
		*	@param batchName Name of the batch in which the texture array will be used
		*	@return A texture corresponding to the texture array
		*/
		TextureData* BuildMaterials(std::vector<Material*>& materials, std::string batchName);
		/**
		*	Build all the mesh in the buffer
		*/
		void BuildMesh();
		/**
		*	Build the skybox of the scene
		*/
		void BuildSkybox();
		/**
		*	Upload all indirect command to the gpu
		*/
		void BuildCommandBuffer();
		/**
		*	Upload light data to the gpu
		*/
		void BuildLights();
		/**
		*	Update lights on the gpu
		*/
		void UpdateLights();
		/**
		*	Update the model of all instances
		*/
		void UpdateModelMatrix();
		/**
		*	Get the current offset in the command buffer
		*	@return The current offset in the command buffer
		*/
		GLuint GetCommandOffset();
		/**
		*	Upload a texture resource in an specific layer of an already bound texture array
		*/
		void UploadTextureToArray(Texture* texture, int layer);

		//Function used to setup attribute or uniform
		void SetupPositionAttribute(RenderData data);
		void SetupTexCoordAttribute(RenderData data);
		void SetupNormalAttribute(RenderData data);
		void SetupModelMatrixUniform(RenderData data);
		void SetupViewMatrixUniform(RenderData data);
		void SetupProjMatrixUniform(RenderData data);
		void SetupInverseViewMatrixUniform(RenderData data);
		void SetupCamPositionUniform(RenderData data);
		void SetupCamTargetUniform(RenderData data);
		void SetupCamUpUniform(RenderData data);
		void SetupViewportUniform(RenderData data);
		void SetupFieldOfViewUniform(RenderData data);
		void SetupDiffuseLayer(RenderData data);
		void SetupSpecularLayer(RenderData data);
		void SetupGlossLayer(RenderData data);
		void SetupMaterialTexture(RenderData data);
		void SetupModelMatrixBlockBindingPoint(RenderData data);
		void SetupModelMatrixBufferBinding(RenderData data);
		void SetupMaterialLayerBlockBindingPoint(RenderData data);
		void SetupMaterialLayerBufferBinding(RenderData data);
		void SetupLightBlockBindingPoint(RenderData data);
		void SetupLightBufferBinding(RenderData data);
		void SetupInstanceId(RenderData data);
		void SetupAmbientLightColor(RenderData data);
		void SetupLightCount(RenderData data);
		void SetupShadowMatrixBlockBindingPoint(RenderData data);
		void SetupShadowMatrixBufferBinding(RenderData data);
		void SetupShadowTexture(RenderData data);
	};
}

#endif