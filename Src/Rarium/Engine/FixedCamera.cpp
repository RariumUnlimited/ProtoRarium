#include <Rarium/Engine/FixedCamera.h>
#include <glm/trigonometric.hpp>
#include <glm/geometric.hpp>

namespace Rarium
{
	FixedCamera::FixedCamera(float fieldOfView, float nearPlaneDistance, float farPlaneDistance, uvec2 viewport, vec3 pos, vec3 lookat, vec3 up) :
		Camera(fieldOfView,nearPlaneDistance,farPlaneDistance,viewport)
	{
		
		this->pos = pos;
		this->lookAt = lookat;
		this->up = up;
	}

	void FixedCamera::Update(float delta, InputState& inputState, InputState& lastInputState)
	{

	}
}