#include <Rarium/Engine/Engine.h>
#include <Rarium/System/Clock.h>
#include <Rarium/ResourceHandling/Catalog.h>
#include <Rarium/Engine/IAppLogic.h>
#include <Rarium/Rendering/RES.h>
#include <Rarium/Engine/Config.h>
namespace Rarium
{
	Engine::Engine(IAppLogic* appLogic)
	{
		this->appLogic = appLogic;

		renderer = new RES;

		controller = nullptr;

		running = true;
		useAxis = false;
		skipMouse = false;
		error = false;
	}

	Engine::~Engine()
	{
		delete renderer;
	}

	void Engine::Run()
	{		
		try
		{
			//Initialize everything, at each step we check running to be sure the previous step go init correctly
			if(InitEngine())
			{
				Log("Engine: Window opened, Width: "+ToString(width)+"px, Height: "+ToString(height)+"px");

				renderer->Init(width,height);

				if (!error)
					appLogic->Initialize(renderer);

				if (!error)
					renderer->Build();

				//Reset the clock for delta time calculation
				Clock::Get().GetElapsedTime();
				try
				{
					while(running && !error)
					{
						ProcessEvents();
						float delta = Clock::Get().GetElapsedTime();
						appLogic->Update(delta);
						renderer->DrawFrame(delta);
						SDL_GL_SwapWindow(mainwindow);
					}
				}
				catch(Exception e)
				{
					ReportError(e.what());
				}

				Catalog::Get().Purge();
				renderer->Purge();

				StopEngine();
			}
		}
		catch(Exception e)
		{
			ReportError(e.what());
		}
	}

	bool Engine::InitEngine()
	{
		if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER) < 0)
		{
			ReportError("Erreur lors de l'initialisation de la SDL : " + std::string(SDL_GetError()));
			SDL_Quit();
			return false;
		}

		//Set sdl gl attribute
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, RGLMajVersion);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, RGLMinVersion);
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, RGLContext );
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,4);
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,24);

		mainwindow = SDL_CreateWindow("Rarium", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			1280, 720, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		width = 1280;
		height = 720;

		if (!mainwindow)
		{
			ReportError("SDL Window creation failed");
			return false;
		}

		maincontext = SDL_GL_CreateContext(mainwindow);

		SDL_GL_SetSwapInterval(1);

		//Check controller status
		for(int i = 0; i < SDL_NumJoysticks(); i++)
		{
			SDL_Joystick *joystick = SDL_JoystickOpen(i);
			Log("Engine: Joystick detected : "+ToString( SDL_JoystickName(joystick)));
			if (SDL_IsGameController(i)) 
			{
				controller = SDL_GameControllerOpen(i);
				if (!controller) 
				{
					ReportError("Could not open gamecontroller");
				}
				else
				{
					Log("Engine: Controller added : "+std::string(SDL_GameControllerName(controller)));
				}
				break;
			}
			else
				controller = nullptr;
		}


		//SDL_ShowCursor(SDL_DISABLE);

		SDL_FlushEvents(SDL_FIRSTEVENT,SDL_LASTEVENT);

		return true;
	}

	void* Engine::GetProcAddress(std::string procName)
	{
		return SDL_GL_GetProcAddress(procName.c_str());
	}

	void Engine::StopEngine()
	{
		SDL_Quit();
	}

	File Engine::ReadFile(std::string fileName)
	{
		File file;
		try
		{
			SDL_RWops *rw = SDL_RWFromFile(("assets/"+fileName).c_str(),"rb");	

			if (rw != nullptr) {
				Sint64 length = SDL_RWseek(rw,0,SEEK_END);

				if (length < 0) {
					Log("Could not seek inside : "+fileName);
					file.Size = 0;
				}
				file.Size = size_t(length);
			}
			else
			{
				throw Rarium::Exception("Could not read file : "+fileName);
			}

			SDL_RWseek(rw,0,RW_SEEK_SET);

			file.Data = new char[file.Size+1];
			//We put an \0 in the case the content of the file is read with a string stream
			file.Data[file.Size] = '\0';

			SDL_RWread(rw, file.Data, file.Size, 1);

			SDL_RWclose(rw);
		}
		catch(std::exception e)
		{
			throw Rarium::Exception("Could not read file : "+fileName);
		}

		return file;
	}

	void Engine::ProcessEvents()
	{
		lastInputState = inputState;

		SDL_Event e;
		while( SDL_PollEvent( &e ) != 0 )
		{
			if( e.type == SDL_QUIT)
				OnWindowClose();
			else if(e.type == SDL_KEYDOWN)
				HandleKeyDown(e);
			else if(e.type == SDL_KEYUP)
				HandleKeyUp(e);
			else if(e.type == SDL_APP_WILLENTERBACKGROUND)
				HandleAppInBackground(e);
			else if(e.type == SDL_MOUSEBUTTONDOWN)
				HandleMouseButtonDown(e);
			else if(e.type == SDL_MOUSEBUTTONUP)
				HandleMouseButtonUp(e);
			else if(e.type == SDL_CONTROLLERBUTTONDOWN)
				HandleControllerButtonDown(e);
			else if(e.type == SDL_CONTROLLERBUTTONUP)
				HandleControllerButtonUp(e);
			else if(e.type == SDL_CONTROLLERAXISMOTION)
				useAxis = true;
		}

		if(!useAxis)
		{
			int mousex,mousey;

			SDL_GetMouseState(&mousex,&mousey);

			inputState.Yaw = Clamp(float(mousex) - float(width)/2,-30.f,30.f) / 30.f;
			inputState.Pitch = Clamp(float(mousey) - float(height)/2,-30.f,30.f) / 30.f;

			//SDL_WarpMouseInWindow(mainwindow,width/2,height/2);
		}

		HandleAxisMotion();
	}

	void Engine::HandleKeyDown(SDL_Event e)
	{
		if(e.key.keysym.sym == SDLK_z)
			inputState.Throttle = 1;
		else if(e.key.keysym.sym == SDLK_s)
			inputState.Throttle = -1;
		else if(e.key.keysym.sym == SDLK_1)
			inputState.Actions[Action::ChangeWeapon] = true;
		else if(e.key.keysym.sym == SDLK_2)
			inputState.Actions[Action::ChangeMissile] = true;
		else if(e.key.keysym.sym == SDLK_x)
			inputState.Actions[Action::LinkGun] = true;
		else if(e.key.keysym.sym == SDLK_RETURN)
			inputState.Actions[Action::Menu] = true;
		else if(e.key.keysym.sym == SDLK_m)
			inputState.Actions[Action::Map] = true;
		else if(e.key.keysym.sym == SDLK_t)
			inputState.Actions[Action::Target] = true;
		else if(e.key.keysym.sym == SDLK_LSHIFT)
			inputState.Actions[Action::Boost] = true;
		else if(e.key.keysym.sym == SDLK_e)
			inputState.Actions[Action::Interact] = true;
		else if(e.key.keysym.sym == SDLK_ESCAPE)
			inputState.Actions[Action::Cancel] = true;
		else if(e.key.keysym.sym == SDLK_a)
			inputState.Actions[Action::SpecialAction] = true;
		else if(e.key.keysym.sym == SDLK_q)
			inputState.Roll = -1;
		else if(e.key.keysym.sym == SDLK_d)
			inputState.Roll = 1;
	}

	void Engine::HandleKeyUp(SDL_Event e)
	{
		if(e.key.keysym.sym == SDLK_z)
			inputState.Throttle = 0;
		else if(e.key.keysym.sym == SDLK_s)
			inputState.Throttle = 0;
		else if(e.key.keysym.sym == SDLK_1)
			inputState.Actions[Action::ChangeWeapon] = false;
		else if(e.key.keysym.sym == SDLK_2)
			inputState.Actions[Action::ChangeMissile] = false;
		else if(e.key.keysym.sym == SDLK_x)
			inputState.Actions[Action::LinkGun] = false;
		else if(e.key.keysym.sym == SDLK_RETURN)
			inputState.Actions[Action::Menu] = false;
		else if(e.key.keysym.sym == SDLK_m)
			inputState.Actions[Action::Map] = false;
		else if(e.key.keysym.sym == SDLK_t)
			inputState.Actions[Action::Target] = false;
		else if(e.key.keysym.sym == SDLK_LSHIFT)
			inputState.Actions[Action::Boost] = false;
		else if(e.key.keysym.sym == SDLK_e)
			inputState.Actions[Action::Interact] = false;
		else if(e.key.keysym.sym == SDLK_ESCAPE)
			inputState.Actions[Action::Cancel] = false;
		else if(e.key.keysym.sym == SDLK_a)
			inputState.Actions[Action::SpecialAction] = false;
		else if(e.key.keysym.sym == SDLK_q)
			inputState.Roll = 0;
		else if(e.key.keysym.sym == SDLK_d)
			inputState.Roll = 0;
	}

	void Engine::HandleAppInBackground(SDL_Event e)
	{
		SDL_Event eTemp;
		for(;;)
		{
			SDL_WaitEvent(&eTemp);
			if(eTemp.type == SDL_APP_WILLENTERFOREGROUND)
				break;
		}

		Clock::Get().GetElapsedTime();
	}

	void Engine::HandleMouseButtonDown(SDL_Event e)
	{
		if(e.button.button == SDL_BUTTON_LEFT)
			inputState.Actions[Action::Fire] = true;
		else if(e.button.button == SDL_BUTTON_RIGHT)
			inputState.Actions[Action::Missile] = true;
	}

	void Engine::HandleMouseButtonUp(SDL_Event e)
	{
		if(e.button.button == SDL_BUTTON_LEFT)
			inputState.Actions[Action::Fire] = false;
		else if(e.button.button == SDL_BUTTON_RIGHT)
			inputState.Actions[Action::Missile] = false;
	}

	void Engine::HandleControllerButtonDown(SDL_Event e)
	{
		if(e.cbutton.button == SDL_CONTROLLER_BUTTON_A)
			inputState.Actions[Interact] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_B)
			inputState.Actions[Cancel] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_X)
			inputState.Actions[Boost] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_Y)
			inputState.Actions[Target] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_BACK)
			inputState.Actions[Map] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_START)
			inputState.Actions[Menu] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_LEFTSHOULDER)
			inputState.Throttle = -1;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_RIGHTSHOULDER)
			inputState.Actions[Missile] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_DOWN)
			inputState.Actions[SpecialAction] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_LEFT)
			inputState.Actions[ChangeWeapon] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_RIGHT)
			inputState.Actions[ChangeMissile] = true;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_UP)
			inputState.Actions[LinkGun] = true;
	}

	void Engine::HandleControllerButtonUp(SDL_Event e)
	{
		if(e.cbutton.button == SDL_CONTROLLER_BUTTON_A)
			inputState.Actions[Interact] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_B)
			inputState.Actions[Cancel] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_X)
			inputState.Actions[Boost] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_Y)
			inputState.Actions[Target] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_BACK)
			inputState.Actions[Map] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_START)
			inputState.Actions[Menu] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_LEFTSHOULDER)
			inputState.Throttle = 0;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_RIGHTSHOULDER)
			inputState.Actions[Missile] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_DOWN)
			inputState.Actions[SpecialAction] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_LEFT)
			inputState.Actions[ChangeWeapon] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_RIGHT)
			inputState.Actions[ChangeMissile] = false;
		else if(e.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_UP)
			inputState.Actions[LinkGun] = false;
	}

	void Engine::HandleAxisMotion()
	{
		if(useAxis)
		{
			Sint16 sdlAxisValue = 0;
			float axisValue = float(sdlAxisValue)/32767.f;

			//Get the axis value and normalized between -1 and 1
			sdlAxisValue = SDL_GameControllerGetAxis(controller,SDL_CONTROLLER_AXIS_LEFTX);
			axisValue = float(sdlAxisValue)/32767.f;

			//Check if we are in the dead zone
			if(axisValue > 0.25f || axisValue < -0.25f)
				inputState.Roll = axisValue;
			else
				inputState.Roll = 0.f;

			sdlAxisValue = SDL_GameControllerGetAxis(controller,SDL_CONTROLLER_AXIS_RIGHTX);
			axisValue = float(sdlAxisValue)/32767.f;

			if(axisValue > 0.25f || axisValue < -0.25f)
				inputState.Yaw = axisValue;
			else
				inputState.Yaw = 0.f;

			sdlAxisValue = SDL_GameControllerGetAxis(controller,SDL_CONTROLLER_AXIS_RIGHTY);
			axisValue = float(sdlAxisValue)/32767.f;

			if(axisValue > 0.25f || axisValue < -0.25f)
				inputState.Pitch = axisValue;
			else
				inputState.Pitch = 0.f;

			sdlAxisValue = SDL_GameControllerGetAxis(controller,SDL_CONTROLLER_AXIS_TRIGGERLEFT);
			axisValue = float(sdlAxisValue)/32767.f;

			if(axisValue > 0.25f)
				inputState.Throttle = axisValue;
			else if(inputState.Throttle > 0)
				inputState.Throttle = 0.f;


			sdlAxisValue = SDL_GameControllerGetAxis(controller,SDL_CONTROLLER_AXIS_TRIGGERRIGHT);
			axisValue = float(sdlAxisValue)/32767.f;

			if(axisValue > 0.25f)
				inputState.Actions[Fire] = true;
			else
				inputState.Actions[Fire] = false;
		}
	}

	void Engine::HandleWindowEvent(SDL_Event e)
	{
		SDL_Event eTemp;
		if(e.window.event ==  SDL_WINDOWEVENT_HIDDEN)
		{
			for(;;)
			{
				SDL_WaitEvent(&eTemp);
				if(eTemp.type == SDL_WINDOWEVENT && eTemp.window.event == SDL_WINDOWEVENT_SHOWN)
					break;
			}
			Clock::Get().GetElapsedTime();
		}
		else if(e.window.event ==  SDL_WINDOWEVENT_RESIZED)
		{
			Log("The window has been resized, it should not for the moment");
			throw Exception("Window resized");		
		}
		else if(e.window.event ==  SDL_WINDOWEVENT_MINIMIZED)
		{
			for(;;)
			{
				SDL_WaitEvent(&eTemp);
				if(eTemp.type == SDL_WINDOWEVENT && eTemp.window.event == SDL_WINDOWEVENT_RESTORED)
					break;
			}
			Clock::Get().GetElapsedTime();
		}
		else if(e.window.event ==  SDL_WINDOWEVENT_LEAVE)
		{
			SDL_WarpMouseInWindow(mainwindow,width/2,height/2);
		}
		else if(e.window.event ==  SDL_WINDOWEVENT_FOCUS_LOST)
		{
			for(;;)
			{
				SDL_WaitEvent(&eTemp);
				if(eTemp.type == SDL_WINDOWEVENT && eTemp.window.event == SDL_WINDOWEVENT_FOCUS_GAINED)
					break;
			}
			Clock::Get().GetElapsedTime();
		}
	}

	void Engine::OnWindowClose()
	{
		Log("Engine: Window wants to close");
		running = false;
	}

	void Engine::OnWindowResize(int width, int height)
	{
		throw Exception("Window should not resize.");
		running = false;
	}

	void Engine::ReportError(std::string error)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"Error",error.c_str(),nullptr);
		error = true;
	}

	InputState Engine::inputState;
	InputState Engine::lastInputState;
	bool Engine::error;
}