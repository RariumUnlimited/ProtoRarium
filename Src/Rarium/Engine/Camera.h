#ifndef _RCAMERA_

#define _RCAMERA_

#include <Rarium/Core.h>
#include <Rarium/Input/InputState.h>
#include <glm/vec3.hpp>

namespace Rarium
{
	///Represent a virtual camera of a scene
	class Camera
	{
	public:
		/**
		*	Build a new camera
		*	@param fieldOfView Field of view of the camera
		*	@param nearPlaneDistance Distance to the near view plane
		*	@param farPlaneDistance Ditance to the far view plane
		*	@param viewport Viewport of the camera
		*/
		Camera(float fieldOfView, float nearPlaneDistance, float farPlaneDistance, uvec2 viewport)
		{
			this->fieldOfView = fieldOfView;
			this->nearPlaneDistance = nearPlaneDistance;
			this->farPlaneDistance = farPlaneDistance;
			pos = vec3(0.0, 0.0, -1.0);
			lookAt = vec3(0.0, 0.0, 0.0);
			up = vec3(0.0, 1.0, 0.0);
			this->viewport = viewport;
		}
		/**
		*	Update the camera position and lookAt
		*	@param delta Seconds since last update
		*	@param inputState State of user input and action
		*/
		virtual void Update(float delta, InputState& inputState, InputState& lastInputState) = 0;
		/**
		*	Get the position of the camera
		*	@return The position of the camera
		*/
		inline vec3 GetPosition() { return pos; }
		/**
		*	Get where the camera look
		*	@return Where the camera look
		*/
		inline vec3 GetLookAt() { return lookAt; }
		/**
		*	Get the up vector for the camera
		*	@return The up vector of the camera
		*/
		inline vec3 GetUp() { return up; }
		/**
		*	Get the viewport of the camera
		*	@return Viewport of the camera
		*/
		inline uvec2 GetViewport() { return viewport; }
		/**
		*	Set the position of the camera
		*	@param position The new position of the camera
		*/
		inline void SetPosition(vec3 position) { this->pos = position; }
		/**
		*	Set where the camera must look
		*	@param lookAt New position of where the camera look
		*/
		inline void SetLookAt(vec3 lookAt) { this->lookAt = lookAt; }
		/**
		*	Set the up vector of the camera
		*	@param up The new up vector of the camera
		*/
		inline void SetUp(vec3 up) { this->up = up; }
		/**
		*	Get the field of view of this camera
		*	@return The field of view of the camera
		*/
		inline float GetFieldOfView() { return fieldOfView; }
		/**
		*	Get the distance to the near plane of the camera
		*	@return The distance to the near plane
		*/
		inline float GetNearPlaneDistance() { return nearPlaneDistance; }
		/**
		*	Get the distance to the far plane of the camera
		*	@return The distance to the far plane
		*/
		inline float GetFarPlaneDistance() { return farPlaneDistance; }
		

	protected:
		float fieldOfView;///< Field of view of the camera in degree
		float nearPlaneDistance;///< Distance to the near plane of the camera
		float farPlaneDistance;///< Distance to the far plane of the camera
		vec3 pos;///< Position of the camera
		vec3 lookAt;///< Where the camera look
		vec3 up;///< The up vector of the camera
		uvec2 viewport;///< Viewport of the camera
	};
}

#endif