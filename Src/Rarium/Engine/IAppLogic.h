#ifndef _RIAPPLOGIC_

#define _RIAPPLOGIC_

#include <Rarium/Core.h>
#include <Rarium/Rendering/IRenderer.h>

namespace Rarium
{
	/**
	*	Implement this interface for you app/game logic
	*/
	class IAppLogic
	{
	public:
		/**
		*	Called by the engine when the app needs to initialize
		*	@param renderer The renderer used by the engine.
		*/
		virtual void Initialize(IRenderer* renderer) = 0;
		/**
		*	Update the app logic for the current frame, when called we are sure that all the resources registered in the catalog are loaded
		*	@param delte Delta time between the last frame and the current one
		*/
		virtual void Update(float delta) = 0;
	};
}

#endif