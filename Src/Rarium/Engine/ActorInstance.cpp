#include <Rarium/Engine/ActorInstance.h>
#include <glm/gtc/matrix_transform.hpp>

namespace Rarium
{
	ActorInstance::ActorInstance(Actor* actor)
	{
		this->actor = actor;
		position = vec3(0.0,0.0,0.0);
	}

	mat4 ActorInstance::GetMatrix()
	{
		return glm::translate(glm::mat4(1.0),position);
	}
}