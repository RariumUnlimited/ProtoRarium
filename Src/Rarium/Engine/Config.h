#ifndef _RCONFIG_

#define _RCONFIG_

#ifdef WIN32

#define RGLMajVersion 4
#define RGLMinVersion 4

#ifndef _DEBUG
#define RGLContext 0x0001
#else
#define RGLContext 0x0002
#endif

#else

#define RGLMajVersion 4
#define RGLMinVersion 4
#define RGLContext SDL_GL_CONTEXT_PROFILE_CORE

#endif

#endif