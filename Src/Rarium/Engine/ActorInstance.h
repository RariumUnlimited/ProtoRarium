#ifndef _RACTORINSTANCE_

#define _RACTORINSTANCE_

#include <Rarium/Core.h>
#include <glm/glm.hpp>
#include <Rarium/Resources/Actor.h>

namespace Rarium
{
	/**
	*	Represent an instance of an actor that can be add to the scene and drawn
	*/
	class ActorInstance
	{
	public:
		/**
		*	Build a new actor instance
		*	@param actor Base actor for this instance
		*/
		ActorInstance(Actor* actor);
		/**
		*	Get the transformation matrix for this instance
		*	@return Transformation matrix for this intance
		*/
		virtual mat4 GetMatrix();
		/**
		*	Get the base actor of this instance
		*	@return Base actor of this instance
		*/
		inline Actor* GetActor() { return actor; }
		/**
		*	Get the position of this instance
		*	@return The position of this instance
		*/
		inline vec3 GetPosition() { return position; }
		/**
		*	Set the position of this instance
		*	@param position New position of this instance
		*/
		inline void SetPosition(vec3 position) { this->position = position; }
		/**
		*	Update the instance
		*	@param delta Delta time since last update
		*/
		virtual void Update(float delta) {}

	protected:
		Actor* actor;///< Base actor
		vec3 position;///< Position of this instance
	};
}

#endif