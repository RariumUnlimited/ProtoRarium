#ifndef _101FIXEDCAMERA_

#define _101FIXEDCAMERA_

#include <Rarium/Core.h>
#include <Rarium/Engine/Camera.h>
#include <glm/vec2.hpp>

namespace Rarium 
{  
	///Represent a virtual freefly camera of a scene
	class FixedCamera : public Camera
	{
	public:
		/**
		*	Build a new camera
		*	@param fieldOfView Field of view of the camera
		*	@param nearPlaneDistance Distance to the near view plane
		*	@param farPlaneDistance Ditance to the far view plane
		*/
		FixedCamera(float fieldOfView, float nearPlaneDistance, float farPlaneDistance, uvec2 viewport, vec3 pos, vec3 lookat, vec3 up);
		/**
		*	Update the camera position and lookAt
		*	@param delta Seconds since last update
		*/
		virtual void Update(float delta, InputState& inputState, InputState& lastInputState);

	private:
	};
}

#endif