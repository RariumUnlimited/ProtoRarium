#ifndef _RENGINE_

#define _RENGINE_

#include <Rarium/Core.h>
#include <Rarium/Input/InputState.h>
#include <Rarium/System/IEventListener.h>
#include <Rarium/System/File.h>

namespace Rarium
{
	class IRenderer;
	class IAppLogic;

	/**
	*	Engine that make everything running
	*/
	class Engine : protected IEventListener
	{
	public:
		/**
		*	Build the engine
		*	@param appLogic Update logic for the user app
		*/
		Engine(IAppLogic* appLogic);
		/**
		*	Destroy our engine
		*/
		~Engine();
		/**
		*	Launch the app
		*/
		void Run();
		/**
		*	Return the adresse of an opengl function
		*	@param procName The name of the function
		*	@return Adress of the function
		*/
		static void* GetProcAddress(std::string procName);
		/**
		*	Get a file from the assets dir
		*	@return Data from the file read, File.Size = 0 if fail
		*/
		static File ReadFile(std::string fileName);
		/**
		*	Get the user input state of the current frame
		*/
		static inline InputState& GetInputState() { return inputState; }
		/**
		*	Get the user input state of the last frame
		*/
		static inline InputState& GetLastInputState() { return lastInputState; }
		/**
		*	Report an error to the engine
		*	@param error The message of the error
		*/
		static void ReportError(std::string error);
	protected:
		/**
		*	Called when the window wants closing
		*/
		void OnWindowClose();
		/**
		*	Call when the window got resized
		*	@param width New width of the window
		*	@param height New height of the window
		*/
		void OnWindowResize(int width, int height);
		/**
		*	Open the window and initialize sdl/opengl
		*	@return True if the window opened successfully
		*/
		bool InitEngine();
		/**
		*	Shut down sdl
		*/
		void StopEngine();
		/**
		*	Process events
		*/
		void ProcessEvents();
		/**
		*	Handle a key down event
		*	@param e The keydown event
		*/
		void HandleKeyDown(SDL_Event e);
		/**
		*	Handle a key up event
		*	@param e The keyup event
		*/
		void HandleKeyUp(SDL_Event e);
		/**
		*	When the app goes in background on mobile
		*	@param e The event triggered
		*/
		void HandleAppInBackground(SDL_Event e);
		/**
		*	Handle a mouse button down event
		*	@param e The mouse button down event
		*/
		void HandleMouseButtonDown(SDL_Event e);
		/**
		*	Handle a mouse button up event
		*	@param e The mouse button up event
		*/
		void HandleMouseButtonUp(SDL_Event e);
		/**
		*	Handle a controller button down event
		*	@param e The controller button down event
		*/
		void HandleControllerButtonDown(SDL_Event e);
		/**
		*	Handle a controller button up event
		*	@param e The controller button up event
		*/
		void HandleControllerButtonUp(SDL_Event e);
		/**
		*	Treat the state of the controller axis
		*/
		void HandleAxisMotion();
		/**
		*	Handle a window event
		*	@param e The window event
		*/
		void HandleWindowEvent(SDL_Event e);

		IAppLogic* appLogic;///< Update logic for the user app
		IRenderer* renderer;///< Renderer used by our app
		
		SDL_Window* mainwindow; ///< Our sdl window
		SDL_GLContext maincontext;///< Our opengl context

		static bool error;///< If true then an error occured and the app must quit

		unsigned int width;///< Width of our window
		unsigned int height;///< Height of our window

		bool running;///< To know if the app is running
		bool useAxis;///< To know if we use a controller or mouse for the yaw pitch roll
		bool skipMouse;///< To know if we must skip mouse update next frame

		SDL_GameController* controller;///< Main game controller of the app

		static InputState inputState;///< Current state of the user input
		static InputState lastInputState;///< State of the user input last frame
	};
}

#endif