#ifndef _REXCEPTION_

#define _REXCEPTION_

#include <string>
#include <stdexcept>

namespace Rarium
{
	/**
	*	Exception that can be send by the engine component
	*/
	class Exception : public std::exception
	{
	public:
		/**
		*	Create a new exception
		*	@param message Message describing what is the exception
		*/
		Exception(std::string message) { this->message = message; }
		virtual const char* what() {return message.c_str();}
	protected:
		std::string message;///< Message describing the exception
	};
}

#endif