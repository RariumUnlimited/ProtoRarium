#ifndef _RUTIL_

#define _RUTIL_

#include <vector>
#include <string>
#include <sstream>

namespace Rarium
{
	/**
	*	Convert an object to a string. T must implement std::string operator<<
	*/
	template <typename T> inline std::string ToString(const T& t) 
	{ 
		std::ostringstream os; 
		os<<t; 
		return os.str(); 
	}

	/**
	*	Split a string in a vector of string depending on a specific character
	*	@param stringToSplit The string that we want to split
	*	@param seperator The char used to know when we need to split
	*	@return A vector containing all the substring of stringToSplit
	*/
	std::vector<std::string> Split(std::string stringToSplit, char separator);

	/**
	*	Template function to clamp a value between a min and a max
	*	@param value Value to clamp
	*	@param min Minimum value
	*	@param max Maximum value
	*	@return min if value < min, max if value > max or value
	*/
	template <typename T> inline T Clamp(T value, T min, T max)
	{
		return value <= max ? (value >= min ? value : min) : max;
	}

	/**
	*	Check if an opengl error occured
	*/
	void TreatGLError(std::string file, std::string line);

#define CheckGLError() TreatGLError(ToString(__FILE__), ToString(__LINE__))
}

#endif