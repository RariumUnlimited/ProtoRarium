#include <GL/glew.h>
#include <Rarium/Util/Util.h>
#include <Rarium/Util/Log.h>
#include <Rarium/Util/Exception.h>

namespace Rarium
{
	std::vector<std::string> Split(std::string stringToSplit, char separator)
	{
		//Where we store the list of strings
		std::vector<std::string> strings;

		//Store the value of the current split string
		std::string currentString;

		for(unsigned int i = 0; i < stringToSplit.size(); i++)
		{
			//If we encounter a space while we are not in an argument it means it's the end of the current argument
			if(stringToSplit[i] == separator)
			{
				if(currentString != "")
				{
					strings.push_back(currentString);
					currentString = "";
				}
			}
			else
			{
				currentString += stringToSplit[i];
			}
		}

		//If we have something in the value of the current, we must add it to the argument list.
		if(currentString != "")
		{
			strings.push_back(currentString);
			currentString = "";
		}

		return strings;
	}

	void TreatGLError(std::string file, std::string line)
	{
		GLenum error = glGetError();

		switch(error)
		{
		case GL_NO_ERROR:
			break;
		case GL_INVALID_ENUM:
			Log("GL Error Occured : " + file + ", line : " + line);
			throw Exception("Received gl error : invalid enum - File : " + Split(file,'\\').back() + " Line : "+line);
			break;
		case GL_INVALID_VALUE:
			Log("GL Error Occured : " + file + ", line : " + line);
			throw Exception("Received gl error : invalid value - File:" + Split(file, '\\').back() + " Line:" + line);
			break;
		case GL_INVALID_OPERATION:
			Log("GL Error Occured : " + file + ", line : " + line);
			throw Exception("Received gl error : invalid operation - File : " + Split(file, '\\').back() + " Line : " + line);
			break;
		case GL_OUT_OF_MEMORY:
			Log("GL Error Occured : " + file + ", line : " + line);
			throw Exception("Received gl error : out of memory - File : " + Split(file, '\\').back() + " Line : " + line);
			break;
		default:
			Log("GL Error Occured : " + file + ", line : " + line);
			throw Exception("Received unknown opengl error");
			break;
		}
	}
}