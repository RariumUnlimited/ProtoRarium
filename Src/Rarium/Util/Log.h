#ifndef _RLOG_

#define _RLOG_

#include <string>

#define Log(message) Rarium::Logger::Get().LogMessage(message)

namespace Rarium
{
	/**
	*	Class used to log message in std::cout. Singleton.
	*/
	class Logger
	{
	public:
		/**
		*	Log a message.
		*	@param message The message
		*/
		void LogMessage(std::string message);

		/**
		*	Get our Logger
		*	@return A reference to the instance of our logger
		*/
		static inline Logger& Get() { return instance; }

	protected:

	private:
		Logger();///< Build a new logger
		static Logger instance;///< Instance of our logger
	};
}

#endif