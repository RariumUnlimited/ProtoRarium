#include <Rarium/Util/Log.h>
#include <iostream>
#include <Rarium/System/Clock.h>
#include <SDL.h>


namespace Rarium
{
	Logger::Logger()
	{
		
	}

	void Logger::LogMessage(std::string message)
	{
		try
		{
			SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION,SDL_LOG_PRIORITY_INFO,message.c_str());
		}
		catch(std::exception e)
		{

		}
	}

	Logger Logger::instance;
}