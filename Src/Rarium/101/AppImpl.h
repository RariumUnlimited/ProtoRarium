#ifndef _101APPIMPL_

#define _101APPIMPL_

#include <Rarium/Engine/IAppLogic.h>
#include <Rarium/Engine/ActorInstance.h>


namespace Rarium
{
	/**
	*	Demo app
	*/
	class App : public Rarium::IAppLogic
	{
	public:
		/**
		*	Destructor
		*/
		~App();

	public:
		/**
		*	Called by the engine when the app needs to initialize
		*	@param renderer The renderer used by the engine.
		*/
		virtual void Initialize(IRenderer* renderer);
		/**
		*	Update the app logic for the current frame, when called we are sure that all the resources registered in the catalog are loaded
		*	@param delte Delta time between the last frame and the current one
		*/
		virtual void Update(float delta);

	protected:
		Actor* moonActor;///< Moon actor
		ActorInstance* moonInstance;///< Moon instance

		Actor* skybox;///< Skybox actor
		ActorInstance* skyboxInstance;///< Skybox instance

		std::vector<Actor*> asteroidActors;///< List of all asteroid actors
		std::vector<ActorInstance*> asteroids;///< List of all asteroid instances
		
		IRenderer* renderer;///< Renderer used by the app
		Camera* camera;///< Camera used by the scene
		Camera* cameraBis;///< Second camera used by the scene
		Light* light;
		Light* lightBis;
	};
}

#endif