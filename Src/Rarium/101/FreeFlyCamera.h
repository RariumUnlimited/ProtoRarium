#ifndef _101FREEFLYCAMERA_

#define _101FREEFLYCAMERA_

#include <Rarium/Engine/Camera.h>
#include <glm/vec2.hpp>

#define VELOCITY 80
#define SENSIVITY 1

namespace Rarium 
{  
	///Represent a virtual freefly camera of a scene
	class FreeFlyCamera : public Camera
	{
	public:
		/**
		*	Build a new camera
		*	@param fieldOfView Field of view of the camera
		*	@param nearPlaneDistance Distance to the near view plane
		*	@param farPlaneDistance Ditance to the far view plane
		*/
		FreeFlyCamera(float fieldOfView, float nearPlaneDistance, float farPlaneDistance, uvec2 viewport);
		/**
		*	Update the camera position and lookAt
		*	@param delta Seconds since last update
		*/
		virtual void Update(float delta, InputState& inputState, InputState& lastInputState);
		/**
		*	@return The forward vector of the camera
		*/
		inline vec3 Forward() { return forward; }

	private:

		float horizontalAngle;
		float verticalAngle;

		vec3 left;
		
		vec3 forward;///< Forward vector of the camera
	};
}

#endif