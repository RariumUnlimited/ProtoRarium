#ifndef _RMOON_

#define _RMOON_

#include <Rarium/Engine/ActorInstance.h>

#define MoonSpeed 0.19634954084f

namespace Rarium
{
	class Moon : public ActorInstance
	{
	public:
		/**
		*	Build a new moon
		*	@param actor The moon actor
		*/
		Moon(Actor* actor);
		/**
		*	Update the instance
		*	@param delta Delta time since last update
		*/
		virtual void Update(float delta);
		/**
		*	Get the transformation matrix for this instance
		*	@return Transformation matrix for this intance
		*/
		virtual mat4 GetMatrix();

	protected:
		float yAngle;///< Angle around the y axis

		vec3 centerVector;///< Translation vector to center this moon mesh
	};

}

#endif