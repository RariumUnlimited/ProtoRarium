#ifndef _RASTEROID_

#define _RASTEROID_

#include <Rarium/Engine/ActorInstance.h>

#define AsteroidSpeed 0.19634954084f

namespace Rarium
{
	//An asteroid
	class Asteroid : public ActorInstance
	{
	public:
		/**
		*	Build a new asteroid from an angle and distance
		*	@param rotationSpeed At which speed the asteroid rotate around itself
		*	@param startAngle The starting angle on the xz plane
		*	@param startDistance Distance from the center
		*	@param actor Asteroid actor
		*/
		Asteroid(float rotationSpeed, float startAngle, float startDistance, Actor* actor);
		/**
		*	Update the instance
		*	@param delta Delta time since last update
		*/
		virtual void Update(float delta);
		/**
		*	Get the transformation matrix for this instance
		*	@return Transformation matrix for this intance
		*/
		virtual mat4 GetMatrix();

	protected:
		float startAngle;///< The starting angle on the xz plane
		float startDistance;///< Distance from the center
		float currentAngle;///< The current angle on the xz plane
		float zAngle;///< Current rotation angle around itself
		float rotationSpeed;///< At which speed it rotate around itself
		vec3 centerVector;///< Translation vector to center this asteroid mesh
	};

}

#endif