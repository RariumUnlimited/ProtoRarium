#include <Rarium/101/Moon.h>
#include <glm/gtc/matrix_transform.hpp>

namespace Rarium
{
	Moon::Moon(Actor* actor) : ActorInstance(actor)
	{
		yAngle = 0;

		//Compute the vector used to center the mesh
		vec3 vertexSum(0, 0, 0);
		for (unsigned int i = 0; i < actor->GetMesh()->GetVertexCount(); i++)
		{
			vertexSum = vertexSum + actor->GetMesh()->GetVertices()[i].Position;
		}

		vertexSum = vertexSum / (float)actor->GetMesh()->GetVertexCount();
		centerVector = -vertexSum;
	}

	void Moon::Update(float delta)
	{
		yAngle += MoonSpeed * delta;
	}

	mat4 Moon::GetMatrix()
	{
		return glm::translate(glm::mat4(1.0), position)*glm::rotate(glm::mat4(1.0), yAngle, vec3(0.0, 1.0, 0.0)) * glm::translate(glm::mat4(1.0), centerVector);
	}
}