#include <Rarium/101/FreeFlyCamera.h>
#include <glm/trigonometric.hpp>
#include <glm/geometric.hpp>

namespace Rarium
{
	FreeFlyCamera::FreeFlyCamera(float fieldOfView, float nearPlaneDistance, float farPlaneDistance, uvec2 viewport) :
		Camera(fieldOfView,nearPlaneDistance,farPlaneDistance,viewport)
	{
		horizontalAngle = 0;
		verticalAngle = 0;
		pos = vec3(0.0f,0.0f,5.0f);
		lookAt = vec3(0.0f,0.0f,0.0f);
		forward = vec3(0.0f,0.0f,1.0f);
		up = vec3(0.0,1.0,0.0);

		verticalAngle = 0.0f;
		horizontalAngle = 180.f;
	}

	void FreeFlyCamera::Update(float delta, InputState& inputState, InputState& lastInputState)
	{
		float sensivity = SENSIVITY * 5;


		float deltaX = inputState.Yaw;
		float deltaY = inputState.Pitch;

		horizontalAngle -= deltaX*sensivity;
		verticalAngle -= deltaY*sensivity;

		if(verticalAngle > 89)
			verticalAngle = 89;
		else if(verticalAngle < -89)
			verticalAngle = -89;

		forward.z = cos(radians(horizontalAngle)) * cos(radians(verticalAngle));
		forward.y = sin(radians(verticalAngle));
		forward.x = sin(radians(horizontalAngle)) * cos(radians(verticalAngle));

		left = normalize(cross(vec3(0.0,1.0,0.0),forward));


		if(inputState.Actions[Action::Interact])
			pos.y+=VELOCITY*delta;
		if(inputState.Actions[Action::SpecialAction])
			pos.y-=VELOCITY*delta;
		if(inputState.Roll > 0)
			pos = pos - (left * (VELOCITY * delta));
		if(inputState.Roll < 0)
			pos = pos + (left * (VELOCITY * delta));
		if(inputState.Throttle < 0)
			pos = pos - (forward * (VELOCITY * delta));
		if(inputState.Throttle > 0)
			pos = pos + (forward * (VELOCITY * delta));

		lookAt = pos+forward;
	}
}