#include <Rarium/Engine/Engine.h>
#include <Rarium/101/AppImpl.h>
#include <Rarium/ResourceHandling/Catalog.h>
#include <Rarium/101/FreeFlyCamera.h>
#include <Rarium/Engine/FixedCamera.h>
#include <Rarium/System/Clock.h>
#include <Rarium/101/Asteroid.h>
#include <Rarium/101/Moon.h>

#include <random>
#include <iostream>

using namespace std;

namespace Rarium
{

	void App::Initialize(IRenderer* renderer)
	{
		moonActor = Catalog::Get().RegisterResource<Actor>("Moon");

		cameraBis = new FixedCamera(45, 0.1f, 2500.f, uvec2(640, 720), vec3(-300.0, 200.0, -300.0), vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
		camera = new FixedCamera(45, 0.1f, 2500.f, uvec2(640, 720), vec3(300.0, 200.0, 300.0), vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));

		std::vector<Material*> asteroidMats;
		std::vector<Mesh*> asteroidMeshes;

		//Load all asteroids mesh and mat
		for (unsigned int i = 1; i <= 30; i++)
		{
			asteroidMeshes.push_back(Catalog::Get().RegisterResource<Mesh>("Asteroid" + ToString(i) + "Mesh"));
		}

		for (unsigned int i = 1; i <= 15; i++)
		{
			asteroidMats.push_back(Catalog::Get().RegisterResource<Material>("Asteroid" + ToString(i) + "Mat"));
		}

		std::default_random_engine generator;
		std::uniform_real_distribution<float> distributionAngle(0,2*pi<float>());
		std::uniform_real_distribution<float> distributionAltitude(-100, 100);
		std::uniform_real_distribution<float> distributionDistance(550, 900);
		std::uniform_real_distribution<float> distributionSpeed(0.78539816339f, 1.57079632679f);

		//Create our 450 asteroid instances
		for (unsigned int i = 0; i < 30; i++)
		{
			for (unsigned int j = 0; j < 15; j++)
			{
				Actor* temp = new Actor("Asteroid"+ToString(i)+"-"+ToString(j));
				temp->SetMesh(asteroidMeshes[i]);
				temp->SetMaterial(asteroidMats[j]);

				bool good = false;

				vec3 position;
				float distance;
				float angle;

				//We are going to loop until the generated coordinates are far enough from other asteroids
				while (!good)
				{
					//Generate a distance and angle from the center
					distance = distributionDistance(generator);
					angle = distributionAngle(generator);

					position = vec3(distance * sin(angle), distributionAltitude(generator), distance * cos(angle));

					good = true;

					for (unsigned int k = 0; k < asteroids.size(); k++)
					{
						vec3 ab = position - asteroids[k]->GetPosition();
						if (dot(ab, ab) < 60.0 * 60.0)
						{
							good = false;
							break;
						}
					}
				}


				ActorInstance* tempInstance = new Asteroid(distributionSpeed(generator), angle, distance, temp);
				tempInstance->SetPosition(position);

				asteroidActors.push_back(temp);
				asteroids.push_back(tempInstance);
				renderer->AddActorInstance(tempInstance);
			}
		}

		this->renderer = renderer;

		skybox = Catalog::Get().RegisterResource<Actor>("SkyboxActor");
		skyboxInstance = new ActorInstance(skybox);
		renderer->SetSkybox(skyboxInstance);

		moonInstance = new Moon(moonActor);
		renderer->AddActorInstance(moonInstance);

		renderer->AddCamera(camera, uvec2(0, 0));
		renderer->AddCamera(cameraBis, uvec2(640, 0));

		renderer->SetAmbientLightColor(vec3(49.f / 255.f, 6.f / 255.f, 24.f / 255.f));

		light = new Light(vec3(0.0, 0.0, -1200.0), vec3(0.0, 0.0, 1.0), vec3(1.0, 1.0, 1.0), radians(10.f),1.0);
		renderer->AddLight(light);

		lightBis = new Light(vec3(1200.0, 0.0, 1200.0), normalize(vec3(-1.0, 0.0, -1.0)), vec3(1.0, 1.0, 1.0), radians(20.f), 1.0);
		
		//renderer->AddLight(lightBis);
	}

	void App::Update(float delta)
	{
		float alpha = Clock::Get().GetTotalTime()*0.15f;
		float beta = Clock::Get().GetTotalTime()*0.25f;

		//Update cameras
		//camera->SetPosition(vec3(1100 * cos(beta) * cos(alpha), 1100 * sin(beta) * cos(alpha), 1100 * sin(alpha)));
		//cameraBis->SetPosition(vec3(1100 * cos(-alpha) * cos(beta), 1100 * sin(-alpha) * cos(beta), 1100 * sin(beta)));

		camera->SetPosition(vec3(0.0, 400.0, 1400.0));
		cameraBis->SetPosition(vec3(0.0, 400.0, -1400.0));

		for(ActorInstance* instance : asteroids)
		{
			instance->Update(delta);
		}
		moonInstance->Update(delta);

	}

	App::~App()
	{
		//Delete every actor and instance

		delete moonInstance;

		for (unsigned int i = 0; i < asteroids.size(); i++)
		{
			delete asteroids[i]->GetActor();
			delete asteroids[i];
		}

		delete skyboxInstance;

		delete camera;
		delete cameraBis;
	}
}