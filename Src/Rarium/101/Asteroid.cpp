#include <Rarium/101/Asteroid.h>
#include <glm/gtc/matrix_transform.hpp>

namespace Rarium
{
	Asteroid::Asteroid(float rotationSpeed, float startAngle, float startDistance, Actor* actor) : ActorInstance(actor)
	{
		this->startAngle = startAngle;
		this->startDistance = startDistance;
		currentAngle = startAngle;
		zAngle = 0;
		this->rotationSpeed = rotationSpeed;

		//Compute the vector used to center the mesh
		vec3 vertexSum(0,0,0);
		for (unsigned int i = 0; i < actor->GetMesh()->GetVertexCount() ; i++)
		{
			vertexSum = vertexSum + actor->GetMesh()->GetVertices()[i].Position;
		}

		vertexSum = vertexSum / (float)actor->GetMesh()->GetVertexCount();
		centerVector = -vertexSum;
	}

	void Asteroid::Update(float delta)
	{
		currentAngle += AsteroidSpeed*delta;

		position = vec3(startDistance * sin(currentAngle), position.y, startDistance * cos(currentAngle));
		zAngle -= rotationSpeed * delta;
	}

	mat4 Asteroid::GetMatrix()
	{
		return glm::translate(glm::mat4(1.0), position)* glm::rotate(glm::mat4(1.0), zAngle, vec3(0.0, 1.0, 0.0)) * glm::rotate(glm::mat4(1.0), zAngle, vec3(0.0, 0.0, 1.0)) * glm::translate(glm::mat4(1.0), centerVector);
	}
}