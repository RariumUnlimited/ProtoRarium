#include <Tools/Converter/Mesh.h>
#include <Rarium/Resources/Mesh.h>
#include <fstream>
#include <iostream>

int main(int argc, char** argv)
{
	std::vector<std::string> objfiles;

	std::ifstream file("listobj.txt",std::ifstream::in);

	char fileLine[1024];

	while(!file.eof())
	{
		//Read the next line of the file, if null then we are at the end of file.
		file.getline(fileLine, sizeof(fileLine));

		std::string line(fileLine);

		if(line.back() == '\n')
			line.pop_back();

		objfiles.push_back(line);
	}

	for(unsigned int i = 0; i < objfiles.size(); i++)
	{
		Mesh* mesh = Mesh::Load("./",objfiles[i]);

		if(mesh->Materials().size() > 1)
		{
			std::vector<Mesh> temp = mesh->SplitObj();
			for(unsigned int j = 0; j < temp.size(); j++)
			{
				temp[j].Process(objfiles[i]+"_"+temp[j].Materials().front().Name);
			}
		}
		else
		{
			mesh->Process(objfiles[i]);

			delete mesh;
		}
	}


	system("pause");

	return 0;
}