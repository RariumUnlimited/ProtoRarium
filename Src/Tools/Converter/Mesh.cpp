#include <Tools/Converter/Mesh.h>
#include <Rarium/Resources/Mesh.h>

#include <iostream>
#include <glm/geometric.hpp>
#include <fstream>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

/**
*	Construct a new mesh
*	@param oId Unique identifier for this mesh
*/
Mesh::Mesh()
{
	
}

void Mesh::BuildFinal()
{
	std::vector<glm::vec3> vertices = this->vertices;
	std::vector<glm::vec3> normals = this->normals;
	std::vector<glm::vec2> texcoords = this->texcoords;
	std::vector<Index> indexes = this->indexes;

	this->vertices.clear();
	this->normals.clear();
	this->texcoords.clear();
	this->indexes.clear();

	std::map<Index, int> remap;
	this->indexes.reserve(indexes.size());

	for(unsigned int i= 0; i < indexes.size(); i++)
	{
		std::pair< std::map<Index, int>::iterator, bool > found= remap.insert( std::make_pair(indexes[i], this->vertices.size()) );

		const int index= found.first->second;
		//~ printf("indices %d %d %d, remap %d\n", indices[i].position, indices[i].texcoord, indices[i].normal, index);
		this->indexes.push_back(index);

		// copier les attributs, si necessaire
		if(found.second)
		{
			this->vertices.push_back(vertices[indexes[i].Vertex]);

			if(indexes[i].TexCoord != -1)
				this->texcoords.push_back(texcoords[indexes[i].TexCoord]);
			else
				this->texcoords.push_back(glm::vec2());

			if(indexes[i].Normal != -1)
				this->normals.push_back(normals[indexes[i].Normal]);
			else
				this->normals.push_back(glm::vec3());
		}
	}
}

static bool eq_f2(const float vA[2], const float vB[2])
{
	return vA[0]==vB[0] && vA[1]==vB[1];
}

static void sub_f2(float res[], const float vA[2], const float vB[2])
{
	res[0]=vA[0]-vB[0]; res[1]=vA[1]-vB[1];
}

static void cpy_f2(float res[], const float vA[2])
{
	res[0]=vA[0]; res[1]=vA[1];
}

static float len_sq_f2(const float vA[2])
{
	return vA[0]*vA[0] + vA[1]*vA[1];
}

void Mesh::SuggestInitialScaleBumpDerivative()
{
	double dSum = 0.0;
	int iNrContributions = 0;
	unsigned int f;

	for ( f=0; f<indexes.size()/3; f++ )
	{
		const int nr_face_verts = 3;		// we only use triangles

		float fST0[] = {texcoords[indexes[f*3+0].Vertex].s, texcoords[indexes[f*3+0].Vertex].t};
		float fST1[] = {texcoords[indexes[f*3+1].Vertex].s, texcoords[indexes[f*3+1].Vertex].t};
		float fST2[] = {texcoords[indexes[f*3+2].Vertex].s, texcoords[indexes[f*3+2].Vertex].t};
		glm::vec3 * ppvVerts[] = { &vertices[indexes[f*3+0].Vertex], &vertices[indexes[f*3+1].Vertex], &vertices[indexes[f*3+2].Vertex], NULL };
		float * ppfTexCoords[] = { fST0, fST1, fST2, NULL};

		assert( nr_face_verts==3 || nr_face_verts==4 );
		if ( nr_face_verts==3 || nr_face_verts==4 )
		{
			const int iNrVerts = (nr_face_verts<3) ? 3 : (nr_face_verts>4 ? 4 : nr_face_verts); 

			// check for degenerate face
			bool bIsDegenerate = false;
			// verify the first 3 vertices are unique
			if(	*ppvVerts[0]==*ppvVerts[1]   || *ppvVerts[0]==*ppvVerts[2]   || *ppvVerts[1]==*ppvVerts[2] ||
				eq_f2(ppfTexCoords[0], ppfTexCoords[1]) || eq_f2(ppfTexCoords[0], ppfTexCoords[2]) || eq_f2(ppfTexCoords[1], ppfTexCoords[2]) )
			{
				bIsDegenerate = true;
			}

			// proceed if not a degenerate face
			if ( bIsDegenerate==false )
			{
				// quads split at shortest diagonal
				// Strictly speaking this is not necessary for this summation
				// but we do it anyway to increase stability in calculations.
				int offs = 0;		// default split is 0,1,2 and 0, 2, 3
				if ( iNrVerts==4 )
				{
					const float fPosLenDiag0 = glm::dot((*ppvVerts[2])-(*ppvVerts[0]),(*ppvVerts[2])-(*ppvVerts[0]));
					const float fPosLenDiag1 = glm::dot((*ppvVerts[3])-(*ppvVerts[1]),(*ppvVerts[3])-(*ppvVerts[1]));
					if(fPosLenDiag1<fPosLenDiag0)
						offs=1;		// alter split
					else if(fPosLenDiag1==fPosLenDiag0)		// do UV check instead
					{
						float fTexcLenDiag0, fTexcLenDiag1;
						float v2Tmp[2];
						sub_f2(v2Tmp, ppfTexCoords[2], ppfTexCoords[0]);
						fTexcLenDiag0=len_sq_f2(v2Tmp);
						sub_f2(v2Tmp, ppfTexCoords[3], ppfTexCoords[1]);
						fTexcLenDiag1=len_sq_f2(v2Tmp);
						if(fTexcLenDiag1<fTexcLenDiag0)
						{
							offs=1;		// alter split
						}
					}
				}

				static const int indices[] = {offs+0, offs+1, offs+2, offs+0, offs+2, (offs+3)&0x3 };

				const int iNrTrisToPile = iNrVerts-2;
				assert( iNrTrisToPile==1 || iNrTrisToPile==2);
				if ( iNrTrisToPile==1 || iNrTrisToPile==2 )
				{
					for (int t=0; t<iNrTrisToPile; t++ )
					{
						glm::vec3 &v0 = *ppvVerts[indices[t*3+0]];
						glm::vec3 &v1 = *ppvVerts[indices[t*3+1]];
						glm::vec3 &v2 = *ppvVerts[indices[t*3+2]];

						float edge_t0[2], edge_t1[2];
						sub_f2(edge_t0, ppfTexCoords[indices[t*3+1]], ppfTexCoords[indices[t*3+0]]);
						sub_f2(edge_t1, ppfTexCoords[indices[t*3+2]], ppfTexCoords[indices[t*3+0]]);
						const float f2xAreaUV = fabsf(edge_t0[0]*edge_t1[1] - edge_t0[1]*edge_t1[0]) /** iBakeWidth * iBakeHeight*/;	// deal with resolution specifics in the shader
						if ( f2xAreaUV>FLT_EPSILON )
						{
							glm::vec3 vN = glm::cross( v1-v0, v2-v0 ); 
							const float f2xSurfArea = glm::length(vN);
							const float fSurfRatio = f2xSurfArea/f2xAreaUV;	// tri area divided by texture area

							++iNrContributions;
							dSum += ((double) fSurfRatio);
						}
					}
				}
			}
		}
	}

	// finalize
	const float fAvgAreaRatio = (iNrContributions>0) ? ((float)(dSum / iNrContributions)) : 1.0f;
	const float fUseAsRenderBumpScale = sqrtf(fAvgAreaRatio);		// use width of average surface ratio as your bump scale

	meshSpecificAutoBumpScale = fUseAsRenderBumpScale;
}

void Mesh::Process(std::string objFile)
{
	BuildFinal();

	unsigned int* index = new unsigned int[Indexes().size()];

	for(unsigned int j = 0; j < Indexes().size(); j++)
	{
		index[j] = Indexes()[j].Vertex;
	}

	Rarium::VertexData* vd = new Rarium::VertexData[Vertices().size()];

	for(unsigned int j = 0; j < Vertices().size(); j++)
	{
		vd[j].Position = Vertices()[j];
		vd[j].Normal = Normals()[j];
		vd[j].TexCoord = TexCoords()[j];
	}

	std::ofstream fileYin("./SaveR/Meshs/"+objFile+"Mesh.rin", std::ofstream::out | std::ofstream::binary);
	fileYin.write((char*)index,sizeof(unsigned int) * Indexes().size());
	fileYin.close();

	std::ofstream fileYve("./SaveR/Meshs/"+objFile+"Mesh.rve", std::ofstream::out | std::ofstream::binary);
	fileYve.write((char*)vd,sizeof(Rarium::VertexData) * Vertices().size());
	fileYve.close();

	std::ofstream fileRds("./SaveR/Meshs/"+objFile+"Mesh.rds");
	fileRds << "@Name=" << objFile << "Mesh" << std::endl;
	fileRds << "@VertexCount=" << Vertices().size() << std::endl;
	fileRds << "@IndexCount=" << Indexes().size() << std::endl;
	fileRds << "@MeshSpecificAutoBumpScale=" << MeshSpecificAutoBumpScale() << std::endl;

	std::ofstream fileRdsMat("./SaveR/Materials/"+objFile+"Mat.rds");
	fileRdsMat << "@Name=" << objFile << "Mat" << std::endl;
	fileRdsMat << "@Diffuse=" << objFile << "Mat" << "/diffuse" << std::endl;
	fileRdsMat << "@Specular=" << objFile << "Mat" << "/specular" << std::endl;
	fileRdsMat << "@Gloss=" << objFile << "Mat" << "/gloss" << std::endl;

	std::string path(".\\SaveR\\Materials\\"+objFile+"Mat");

	std::wstring stemp = std::wstring(path.begin(), path.end());

	LPCWSTR winPath = stemp.c_str();

	CreateDirectory(winPath,NULL);

	std::ofstream fileRdsDiffuse("./SaveR/Materials/"+objFile+"Mat/diffuse.rds");
	fileRdsDiffuse << "@Name=" << objFile << "Diffuse" << std::endl;
	std::ofstream fileRdsSpecular("./SaveR/Materials/"+objFile+"Mat/specular.rds");
	fileRdsSpecular << "@Name=" << objFile << "Specular" << std::endl;
	std::ofstream fileRdsGloss("./SaveR/Materials/"+objFile+"Mat/gloss.rds");
	fileRdsGloss << "@Name=" << objFile << "Gloss" << std::endl;

	std::ofstream fileRdsActor("./SaveR/Actors/"+objFile+".rds");
	fileRdsActor << "@Name=" << objFile << std::endl;
	fileRdsActor << "@Mesh=" << objFile << "Mesh" << std::endl;
	fileRdsActor << "@Material=" << objFile << "Mat" << std::endl;
	fileRdsActor << "@Technique=BasicMesh" << std::endl;

	

	delete[] index;
	delete[] vd;
}