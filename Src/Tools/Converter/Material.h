#ifndef _ARK_MATERIAL_

#define _ARK_MATERIAL_

#include <string>
#include <glm/vec3.hpp>


///Represent the material of a mesh part
struct Material
{
	std::string Name;///< Name of the material
	glm::vec3 AmbientColor;///< Ambient color of the material, value are between 0 and 1
	glm::vec3 DiffuseColor;///< Diffuse color of the material, value are between 0 and 1
	glm::vec3 SpecularColor;///< Specular color of the material, value are between 0 and 1
	float SpecularCoefficient;///< Specular coefficient of the material
	std::string DiffuseTextureFilename;///< Diffuse texture of the material

	/**
	*	Get a default material
	*	@return a default material
	*/
	static Material Default()
	{
		Material default;
		default.AmbientColor = glm::vec3(0.2f,0.2f,0.2f);
		default.DiffuseColor = glm::vec3(1.f,1.f,1.f);
		default.SpecularColor = glm::vec3(0.0f,0.0f,0.0f);
		default.SpecularCoefficient = 10;
		default.Name = "DefaultMaterial";
		default.DiffuseTextureFilename = "";
		return default;
	}
};

#endif