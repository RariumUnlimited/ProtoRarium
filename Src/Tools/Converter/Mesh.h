#ifndef _ARK_MESH_

#define _ARK_MESH_

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include <map>
#include <Tools/Converter/Material.h>

///Represent an index
struct Index
{
	int Vertex; ///< Index of the vertex in the vertex array
	int Normal; ///< Index of the normal in the normal array
	int TexCoord; ///< Index of the texcoord in the texcoord arra
	int Material; ///< Index of the material in the material array

	/**
	*	Build a new Index
	*	@param vertex Index of the vertex
	*	@param normal Optionnal index of a normal
	*/
	Index( const int vertex, const int normal = -1, const int texcoord = -1, const int material = -1 )
	{
		this->Vertex = vertex;
		this->Normal = normal;
		this->TexCoord = texcoord;
		this->Material = material;
	}

	bool operator< ( const Index& b ) const
	{
		// ordre lexicographique pour comparer le triplet d'indices
		if(Material != b.Material)
			return Material < b.Material;
		else if(Vertex != b.Vertex)
			return (Vertex < b.Vertex);
		if(TexCoord != b.TexCoord)
			return (TexCoord < b.TexCoord);

		return (Normal < b.Normal);
	}
};

///Represent a mesh : vertices and normals only
class Mesh
{
public:
	Mesh();

	std::vector<Mesh> SplitObj();
	void BuildFinal();
	void SuggestInitialScaleBumpDerivative();
	void Process(std::string objFile);

	inline std::vector<glm::vec3>& Vertices() { return vertices; }///< Get a reference to the vertices
	inline std::vector<glm::vec3>& Normals() { return normals; }///< Get a reference to the normals
	inline std::vector<glm::vec2>& TexCoords() { return texcoords; }///< Get a reference to the texcoords
	inline std::vector<Material>& Materials() { return materials; }///< Get a reference to the materials
	inline std::vector<Index>& Indexes() { return indexes; }///< Get a reference to the indexes
	inline float MeshSpecificAutoBumpScale() { return meshSpecificAutoBumpScale; }

	////////////////////////////////////////////
	//			Geometry Transformation function (will modify vertex)
	//			GT = Geometry Transform
	////////////////////////////////////////////


	static Mesh* Load(std::string directoryName, std::string modelName);
	static std::map<std::string,Material> LoadMtl(std::string directoryName, std::string mtlName);

private:
	std::vector<glm::vec3> vertices; ///< List of all vertices of the mesh
	std::vector<glm::vec3> normals; ///< List of all normals for this mesh
	std::vector<glm::vec2> texcoords; ///< List of all texture coordinates for this mesh
	std::vector<Material> materials; ///< List of all materials that are used in this mesh
	std::vector<Index> indexes; ///< List of indexes for this mesh faces

	float meshSpecificAutoBumpScale;
};

#endif