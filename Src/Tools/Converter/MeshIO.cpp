#include <Tools/Converter/Mesh.h>
#include <iostream>
#include <map>
#include <Tools/Converter/Material.h>
#include <glm/geometric.hpp>
#include <fstream>


std::vector<std::string> Split(std::string stringToSplit, char separator = ' ')
{
	//Where we store the list of strings
	std::vector<std::string> strings;

	//Store the value of the current split string
	std::string currentString;

	for(unsigned int i = 0; i < stringToSplit.size(); i++)
	{
		//If we encounter a space while we are not in an argument it means it's the end of the current argument
		if(stringToSplit[i] == separator)
		{
			if(currentString != "")
			{
				strings.push_back(currentString);
				currentString = "";
			}
		}
		else
		{
			currentString += stringToSplit[i];
		}
	}

	//If we have something in the value of the current, we must add it to the argument list.
	if(currentString != "")
	{
		strings.push_back(currentString);
		currentString = "";
	}

	return strings;
}
/**
*	Load an obj from a file. Mesh must be triangulate.
*	@param oId The uniq id of the mesh to create
*	@param directoryName Path to the directory that contains the model filename
*	@param modelName Name of the model to load (fileName with the extention .obj)
*	@return the new created mesh
*/
Mesh* Mesh::Load(std::string directoryName, std::string modelName)
{

	if(directoryName.back() != '/')
		directoryName+= '/';

	Mesh* mesh = new Mesh();
	std::map<std::string,int> materialIndex;

	//Open the file and check for errors.
	std::ifstream file(directoryName+modelName+".obj", std::ifstream::in);

	if(!file.good())
	{
		return nullptr;
	}

	int currentMaterial = -1;

	std::cout << "Begin obj : "+modelName << std::endl;

	char fileLine[1024];//Store a line of of the file

	while(!file.eof() &&  file.good())
	{
		//Read the next line of the file, if null then we are at the end of file.
		file.getline(fileLine, sizeof(fileLine));

		//Check if there is double space in the string, in case there is we replace it by only one space. (Neccessary for parsing the file)
		std::string line(fileLine);
		size_t spacePos = line.find("  ");
		while(spacePos != line.npos)
		{
			line = line.replace(spacePos, 2, " ");
			spacePos = line.find("  ");
		}

		if(line.size() == 0)
			continue;

		if(line.back() == '\n')
			line.pop_back();

		if(line.size() == 0)
			continue;

		std::vector<std::string> splits = Split(line);

		if(splits[0] == "mtllib")
		{
			std::map<std::string,Material> materialsTemp = LoadMtl(directoryName,modelName);
			for(std::map<std::string,Material>::iterator it = materialsTemp.begin(); it != materialsTemp.end(); it++)
			{
				materialIndex.insert(std::make_pair(it->first,mesh->Materials().size()));
				mesh->Materials().push_back(it->second);
			}
		}

		if(splits[0] == "usemtl")
		{
			std::map<std::string,int>::iterator found = materialIndex.find(splits[1]);
			if(found != materialIndex.end())
				currentMaterial = found->second;
		}
		if(splits[0] == "v") //Nothing after v = Vertex
		{
			float x,y,z;
			if(sscanf_s(line.c_str(), "v %f %f %f", &x, &y, &z) != 3)
				break;
			mesh->Vertices().push_back( glm::vec3(x, y, z) );
		}
		else if(splits[0] == "vn") //n after = Normal
		{
			float x, y, z;
			if(sscanf_s(line.c_str(), "vn %f %f %f", &x, &y, &z) != 3)
				break;
			glm::vec3 normal(x, y, z);
			normal = glm::normalize(normal);
			mesh->Normals().push_back( normal );
		}
		else if(splits[0] == "vt") //n after = Normal
		{
			float x, y;
			if(sscanf_s(line.c_str(), "vt %f %f", &x, &y) != 2)
				break;
			glm::vec2 texCoord(x, y);
			mesh->TexCoords().push_back( texCoord );
		}
		else if(splits[0] == "f") //F = face
		{
			int ia, ib, ic, id; //Store vertex indexes
			int ita, itb, itc, itd; //Store normal indexes
			int ina, inb, inc, ind; //Store texcoord indexes (use only because we can have textcoord even if we don't want it)


			if(sscanf_s(line.c_str(), "f %d %d %d %d", 
				&ia,&ib,&ic, &id) == 4) //vertex, texcoord and normal quads
			{
				mesh->Indexes().push_back( Index(ia-1, -1, -1,currentMaterial) );
				mesh->Indexes().push_back( Index(ib-1, -1, -1,currentMaterial) );
				mesh->Indexes().push_back( Index(ic-1, -1, -1,currentMaterial) );

				mesh->Indexes().push_back( Index(ia-1, -1, -1,currentMaterial) );
				mesh->Indexes().push_back( Index(ic-1, -1, -1,currentMaterial) );
				mesh->Indexes().push_back( Index(id-1, -1, -1,currentMaterial) );
			}
			else if(sscanf_s(line.c_str(), "f %d %d %d", 
				&ia, &ib, &ic) == 3) //If we scan what we want : only vertex
			{
				mesh->Indexes().push_back( Index(ia-1,-1,-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ib-1,-1,-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ic-1,-1,-1,currentMaterial) );
			}
			else if(sscanf_s(line.c_str(), "f %d/%d %d/%d %d/%d", 
				&ia, &ita, &ib, &itb, &ic, &itc) == 6) // Else vertex and texcoord
			{
				mesh->Indexes().push_back( Index(ia-1,-1,ita-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ib-1,-1,itb-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ic-1,-1,itc-1,currentMaterial) );
			}
			else if(sscanf_s(line.c_str(), "f %d//%d %d//%d %d//%d", 
				&ia, &ina, &ib, &inb, &ic, &inc) == 6) //vertex and normal
			{
				mesh->Indexes().push_back( Index(ia-1, ina-1,-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ib-1, inb-1,-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ic-1, inc-1,-1,currentMaterial) );
			}

			else if(sscanf_s(line.c_str(), "f %d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d", 
				&ia, &ita, &ina, &ib, &itb, &inb, &ic, &itc, &inc, &id, &itd, &ind) == 12) //vertex, texcoord and normal quads
			{
				mesh->Indexes().push_back( Index(ia-1, ina-1, ita-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ib-1, inb-1, itb-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ic-1, inc-1, itc-1,currentMaterial) );

				mesh->Indexes().push_back( Index(ia-1, ina-1, ita-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ic-1, inc-1, itc-1,currentMaterial) );
				mesh->Indexes().push_back( Index(id-1, ind-1, itd-1,currentMaterial) );
			}
			else if(sscanf_s(line.c_str(), "f %d/%d/%d %d/%d/%d %d/%d/%d", 
				&ia, &ita, &ina, &ib, &itb, &inb, &ic, &itc, &inc) == 9) //vertex, texcoord and normal
			{
				mesh->Indexes().push_back( Index(ia-1, ina-1, ita-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ib-1, inb-1, itb-1,currentMaterial) );
				mesh->Indexes().push_back( Index(ic-1, inc-1, itc-1,currentMaterial) );
			}
		}
	}//For

	file.close();

	return mesh;
}

std::map<std::string,Material> Mesh::LoadMtl(std::string directoryName, std::string mtlName)
{
	std::map<std::string,Material> materials;
	std::ifstream file(directoryName+mtlName+".mtl", std::ifstream::in);

	std::cout << "Mtl reading "+mtlName << std::endl;

	Material material = Material::Default();

	char fileLine[1024];//Store a line of of the file
	bool error = true;//Check if there is an error after reading the file
	while(!file.eof())
	{
		//Read the next line of the file, if null then we are at the end of file.
		file.getline(fileLine, sizeof(fileLine));

		//Check if there is double space in the string, in case there is we replace it by only one space. (Neccessary for parsing the file)
		std::string line(fileLine);
		size_t spacePos = line.find("  ");
		while(spacePos != line.npos)
		{
			line = line.replace(spacePos, 2, " ");
			spacePos = line.find("  ");
		}

		if(line.size() == 0)
			continue;

		if(line.back() == '\n')
			line.pop_back();

		if(line.size() == 0)
			continue;

		std::vector<std::string> splits = Split(line);

		if(splits[0] == "newmtl")
		{
			if(material.Name != "DefaultMaterial")
				materials.insert(make_pair(material.Name,material));

			material = Material::Default();
			material.Name = splits[1];
		}
		else if(splits[0] == "Ka")
		{
			float x,y,z;
			if(sscanf_s(line.c_str(), "Ka %f %f %f", &x, &y, &z) != 3)
				break;
			glm::vec3 ka = glm::vec3(x,y,z);
			material.AmbientColor = ka;
		}
		else if(splits[0] == "Kd")
		{
			float x,y,z;
			if(sscanf_s(line.c_str(), "Kd %f %f %f", &x, &y, &z) != 3)
				break;
			glm::vec3 kd = glm::vec3(x,y,z);
			material.DiffuseColor = kd;
		}
		else if(splits[0] == "Ks")
		{
			float x,y,z;
			if(sscanf_s(line.c_str(), "Ks %f %f %f", &x, &y, &z) != 3)
				break;
			glm::vec3 ks = glm::vec3(x,y,z);
			material.SpecularColor = ks;
		}
		else if(splits[0] == "Ns")
		{
			float ns;
			if(sscanf_s (line.c_str(), "Ns %f", &ns) != 1)
				break;
			material.SpecularCoefficient = ns;
		}
		else if(splits[0] == "map_Kd")
		{
			material.DiffuseTextureFilename = directoryName+splits[1];
		}
	}

	if(material.Name != "ArkDefaultMaterial")
		materials.insert(make_pair(material.Name,material));
	std::cout << "Mtl end "+mtlName << std::endl;
	return materials;
}

std::vector<Mesh> Mesh::SplitObj()
{
	std::map<int,std::vector<Index>> indexByMat;

	std::cout << "Beggining split processing" << std::endl;


	for(unsigned int i = 0; i < indexes.size(); i++)
	{
		indexByMat[indexes[i].Material].push_back(indexes[i]);
	}

	indexes.clear();

	std::vector<Mesh> meshes;

	for(std::map<int,std::vector<Index>>::iterator it = indexByMat.begin(); it != indexByMat.end(); it++)
	{
		std::cout << "Begin split : "+materials[it->first].Name << std::endl;
		std::map<int,int> oldToNewVertex;
		std::map<int,int> oldToNewNormal;
		std::map<int,int> oldToNewTex;

		Mesh mesh;

		mesh.materials.push_back(materials[it->first]);

		std::vector<Index> matIndex = it->second;

		for(unsigned int i = 0; i < matIndex.size(); i++)
		{
			Index itemp = matIndex[i];
			Index toInsert(-1);
			std::map<int,int>::iterator foundVertex = oldToNewVertex.find(itemp.Vertex);
			std::map<int,int>::iterator foundNormal = oldToNewNormal.find(itemp.Normal);
			std::map<int,int>::iterator foundTex = oldToNewTex.find(itemp.TexCoord);

			if(foundVertex != oldToNewVertex.end())
			{
				toInsert.Vertex = foundVertex->second;
			}
			else
			{
				oldToNewVertex.insert(std::make_pair(itemp.Vertex,mesh.vertices.size()));
				toInsert.Vertex = mesh.vertices.size();
				mesh.vertices.push_back(vertices[itemp.Vertex]);
			}

			if(foundNormal != oldToNewNormal.end())
			{
				toInsert.Normal = foundNormal->second;
			}
			else
			{
				oldToNewNormal.insert(std::make_pair(itemp.Normal,mesh.normals.size()));
				toInsert.Normal = mesh.normals.size();
				mesh.normals.push_back(normals[itemp.Normal]);
			}

			if(foundTex != oldToNewTex.end())
			{
				toInsert.TexCoord = foundTex->second;
			}
			else
			{
				oldToNewTex.insert(std::make_pair(itemp.TexCoord,mesh.texcoords.size()));
				toInsert.TexCoord = mesh.texcoords.size();
				mesh.texcoords.push_back(texcoords[itemp.TexCoord]);
			}

			mesh.indexes.push_back(toInsert);
		}

		meshes.push_back(mesh);
	}
	return meshes;
}
