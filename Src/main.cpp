#include <SDL.h>

#include <Rarium/Util/Log.h>
#include <Rarium/Util/Util.h>
#include <Rarium/Util/Exception.h>

#include <Rarium/101/AppImpl.h>
#include <Rarium/Engine/Engine.h>

#include <Rarium/Rendering/Light.h>



int main(int argc, char *argv[])
{
	Rarium::App* app = new Rarium::App;

	Rarium::Engine* engine = new Rarium::Engine(app);
	try
	{
		engine->Run();
	}
	catch(Rarium::Exception e)
	{
		Log("pour");
	}

	delete engine;

	delete app;

	//_CrtDumpMemoryLeaks();
#ifdef WIN32
	system("pause");
#endif
    return 0;
}