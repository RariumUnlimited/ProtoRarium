#version 440
#extension GL_ARB_shader_draw_parameters : require

in vec4 Position;
in vec2 TexCoord;
in vec3 Normal;

out VsOutBlock
{
	vec2 TexCoord;
	vec3 WorldPos;
	vec3 Normal;
	flat uint DrawId;
} VsOut;

layout(std430) buffer ModelMatrixBlock
{
	mat4 ModelMatrices[];
};

uniform mat4 ViewMatrix;
uniform mat4 ProjMatrix;

void main()
{
	VsOut.TexCoord = vec2(TexCoord.x,TexCoord.y);

	vec4 worldPos = ModelMatrices[gl_DrawIDARB] * Position;
	
	vec4 P = ViewMatrix * worldPos;

	VsOut.WorldPos = worldPos.xyz;

	VsOut.Normal = mat3(ModelMatrices[gl_DrawIDARB]) * Normal;

    gl_Position = ProjMatrix * P;
	
	VsOut.DrawId = gl_DrawIDARB;
}