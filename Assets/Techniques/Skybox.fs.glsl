#version 440

out vec4 color;
layout (depth_greater) out float gl_FragDepth;

uniform sampler2DArray Materials;
uniform int Diffuse;

in VsOutBlock
{
	vec2 TexCoord;
} FsIn;

void main()
{
	gl_FragDepth = 1.0;
	color = texture(Materials,vec3(FsIn.TexCoord,float(Diffuse)));
}