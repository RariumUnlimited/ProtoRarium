#version 440
#extension GL_ARB_shader_draw_parameters : require

in vec4 Position;

layout(std430) buffer ModelMatrixBlock
{
	mat4 ModelMatrices[];
};

uniform mat4 ViewMatrix;
uniform mat4 ProjMatrix;

out float pass_Z;

void main()
{
	vec4 worldPos = ModelMatrices[gl_DrawIDARB] * Position;
	
	vec4 P = ViewMatrix * worldPos;
	
	

    gl_Position = ProjMatrix * P;
	
	pass_Z = gl_Position.z;
}