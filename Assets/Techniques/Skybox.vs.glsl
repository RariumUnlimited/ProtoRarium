#version 440

in vec4 Position;
in vec2 TexCoord;


out VsOutBlock
{
	vec2 TexCoord;
} VsOut;

uniform vec3 CamPosition;
uniform mat4 ViewMatrix;
uniform mat4 ProjMatrix;

void main()
{
					   
	vec3 pos = Position.xyz;
	pos += CamPosition;
	
	VsOut.TexCoord = TexCoord;
	
	gl_Position = ProjMatrix * ViewMatrix * vec4(pos,1.0);
}