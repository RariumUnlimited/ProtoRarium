#version 440

out vec4 color;

uniform sampler2DArray Materials;
uniform sampler2DArrayShadow Shadows;
uniform vec3 AmbientLightColor;
uniform uint LightCount;
uniform vec3 CamPosition;

struct MaterialStruct
{
	int Diffuse;
	int Specular;
	int Gloss;
};

struct LightStruct
{
	vec3 Position;
	float Angle;
	vec3 Direction;
	float CosAngle;
	vec3 Color;
	float Attenuation;
};

layout(std430) buffer MaterialLayerBlock
{
	MaterialStruct MaterialLayers[];
};

layout(std430) buffer LightBlock
{
	LightStruct Lights[];
};

layout(std430) buffer ShadowMatrixBlock
{
	mat4 ShadowMatrices[];
};

layout(std430) buffer ModelMatrixBlock
{
	mat4 ModelMatrices[];
};

in VsOutBlock
{
	vec2 TexCoord;
	vec3 WorldPos;
	vec3 Normal;
	flat uint DrawId;
} FsIn;

vec3 GetLightColor(vec3 lightDirection, vec3 worldPos, vec3 camPos, vec3 normal, vec3 diffuseColor, vec3 specColor, float shininess, LightStruct light)
{
    float diffuseFactor = dot(normal, -lightDirection);

    vec3 diffuse = vec3(0, 0, 0);
    vec3 specular = vec3(0, 0, 0);

    if (diffuseFactor > 0) {
        diffuse = light.Color * diffuseFactor * diffuseColor;
        vec3 vertexToEye = normalize(camPos - worldPos);
        vec3 lightReflect = normalize(reflect(lightDirection, normal));
        float specularFactor = dot(vertexToEye, lightReflect);
        specularFactor = pow(specularFactor, shininess);
        if (specularFactor > 0) {
            specular = light.Color *
            specColor * specularFactor;
        }
    }

    return (diffuse + specular);
}

vec3 GetPointLightColor(vec3 worldPos, vec3 camPos, vec3 normal, vec3 diffuseColor, vec3 specColor, float shininess, LightStruct light)
{
    vec3 lightDirection = worldPos - light.Position;
    float dist = length(lightDirection);
    lightDirection = normalize(lightDirection);

    vec3 col = GetLightColor(lightDirection,worldPos,camPos,normal,diffuseColor,specColor,shininess,light);
    float Attenuation = light.Attenuation;

    return col / Attenuation;
}

vec3 GetSpotLightColor(vec3 worldPos, vec3 camPos, vec3 normal, vec3 diffuseColor, vec3 specColor, float shininess, LightStruct light)
{	
	vec3 lightToPixel = normalize(worldPos - light.Position);
    float spotFactor = dot(lightToPixel, light.Direction);

    if (spotFactor > light.CosAngle) {
        vec3 Color = GetPointLightColor(worldPos,camPos,normal,diffuseColor,specColor,shininess,light);
        return Color * (1.0 - (1.0 - spotFactor) * 1.0/(1.0 - light.CosAngle));
    }
    else {
        return vec3(0,0,0);
    }
}

void main()
{
	vec3 diffuseColor = texture(Materials,vec3(FsIn.TexCoord,float(MaterialLayers[FsIn.DrawId].Diffuse))).rgb;
	vec3 specColor = texture(Materials,vec3(FsIn.TexCoord,float(MaterialLayers[FsIn.DrawId].Specular))).rgb;
	float shininess = texture(Materials,vec3(FsIn.TexCoord,float(MaterialLayers[FsIn.DrawId].Gloss))).r * 128.0;

	vec3 N = normalize(FsIn.Normal);
    
	vec3 finalColor = vec3(0.0,0.0,0.0);
	
	for(uint i = 0; i < LightCount; i++)
	{
		vec4 shadowCoordHom = ShadowMatrices[i] * ModelMatrices[FsIn.DrawId] * vec4(FsIn.WorldPos,1.0);
		shadowCoordHom.xyz /= shadowCoordHom.w;
		shadowCoordHom.xyz = shadowCoordHom.xyz * 0.5 + 0.5;
		shadowCoordHom.z *= 0.9999;
		
		vec4 shadowCoord = vec4(shadowCoordHom.x, shadowCoordHom.y, i, shadowCoordHom.z);
	
		float shadow = texture(Shadows, shadowCoord);
		
		float counterShadow = min(floor(shadowCoord.w), 1.f);
		shadow = mix(shadow, 1.f, counterShadow);
		
		finalColor += GetSpotLightColor(FsIn.WorldPos,CamPosition,N,diffuseColor,specColor,shininess,Lights[i]) * shadow;
	}
	
	finalColor += diffuseColor * AmbientLightColor;
	
	color = vec4(finalColor,1.0);
}