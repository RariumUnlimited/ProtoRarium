#version 440

out vec4 color;

in float pass_Z;

void main()
{
	color = vec4(pass_Z / 2000,0.0,0.0,1.0);
}